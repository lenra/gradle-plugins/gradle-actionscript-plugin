package {
	import fr.lenra.MyClass;
	import flash.display.Sprite;
	import flash.system.WorkerDomain;
	import flash.utils.ByteArray;
	
	public class Main extends Sprite {
		[Embed(source="myWorker.swf", mimeType="application/octet-stream")]
		private static var MyWorker:Class;

		public function Main() {
			trace("Main class");
			new MyClass();
			trace("Worker : "+DEPENDENCY::myWorker);
			trace("Create worker ba : "+MyWorker);
			var ba:ByteArray = new MyWorker();
			trace("ba length : "+ba.length);
			trace("start worker");
			WorkerDomain.current.createWorker(ba).start();
		}
	}
}