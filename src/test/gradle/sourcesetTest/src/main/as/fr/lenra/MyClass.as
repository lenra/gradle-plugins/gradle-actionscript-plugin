package fr.lenra {
	public class MyClass {

		[Embed(source="../../test.txt")]
		private static var test:String;
		
		public function MyClass() {
			trace("ma class + "+test);
		}

		public function get text():String {
			return test;
		}
	}
}