package {
	import fr.lenra.MyClass;
	import flash.display.Sprite; 
    import flash.text.TextField;
	
	public class Main extends Sprite {
		public function Main() {
			var textField:TextField = new TextField(); 
            textField.text = "Hello, World!"; 
			COMPILE::FLASH {
			textField.text = "coucou flash";
			}
			COMPILE::AIR {
			textField.text = "coucou air";
			}
			COMPILE::JS {
			textField.text = "coucou js";
			}
            stage.addChild(textField);
			textField.text += " "+new MyClass().text;
		}
	}
}