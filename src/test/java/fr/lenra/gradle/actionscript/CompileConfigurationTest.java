package fr.lenra.gradle.actionscript;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Test;

public class CompileConfigurationTest {
	@Test
	public void testSerialization() throws IOException, ClassNotFoundException {
		Project project = ProjectBuilder.builder().build();
		CompileConfiguration config = new CompileConfiguration(project, null);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bout);
		out.writeObject(config);
		out.close();
		assertNotNull("Can't happen", bout.toByteArray());
	}
}