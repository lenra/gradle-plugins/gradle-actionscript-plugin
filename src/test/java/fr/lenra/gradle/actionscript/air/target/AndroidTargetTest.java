package fr.lenra.gradle.actionscript.air.target;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Test;

public class AndroidTargetTest {
	@Test
	public void testSerialization() throws IOException, ClassNotFoundException {
		Project project = ProjectBuilder.builder().build();
		AndroidTarget target = new AndroidTarget(project, null);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bout);
		out.writeObject(target);
		out.close();
		assertNotNull("Can't happen", bout.toByteArray());
	}
}