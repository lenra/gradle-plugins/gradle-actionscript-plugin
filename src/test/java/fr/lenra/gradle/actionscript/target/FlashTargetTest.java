package fr.lenra.gradle.actionscript.target;

import java.util.HashMap;
import java.util.Map;

import org.gradle.api.internal.project.DefaultProject;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Assert;
import org.junit.Test;

import fr.lenra.gradle.plugin.ActionScriptExtension;
import fr.lenra.gradle.task.Swf;

public class FlashTargetTest {

    @Test
    public void testDefinesOverride() {
        DefaultProject project = (DefaultProject) ProjectBuilder.builder().build();
        project.getPluginManager().apply("fr.lenra.gradle.actionscript");
        ActionScriptExtension actionscript = project.getExtensions().getByType(ActionScriptExtension.class);
        Assert.assertNotNull("actionscript extension not found by type ActionScriptExtension", actionscript);

        // Main configuration
        Map<String, String> defines = new HashMap<>();
        defines.put("swf", "false");
        defines.put("flash", "false");
        actionscript.getConfiguration().setDefines(defines);

        // SWF target configuration
        AbstractTargetCompiler target = actionscript.getTargets().maybeCreate("swf");
        Assert.assertEquals("The defined build constant 'swf' is not correct", "false", target.getDefines().get("swf"));
        Assert.assertEquals("The defined build constant 'flash' is not correct", "false",
                target.getDefines().get("flash"));
        defines = new HashMap<>();
        defines.put("swf", "true");
        target.setDefines(defines);
        Assert.assertEquals("The defined build constant 'swf' hasn't changed", "true", target.getDefines().get("swf"));

        // Flash target configuration
        target = actionscript.getTargets().maybeCreate("flash");
        Assert.assertEquals("The defined build constant 'swf' is not correct", "true", target.getDefines().get("swf"));
        Assert.assertEquals("The defined build constant 'flash' is not correct", "false",
                target.getDefines().get("flash"));
        defines = new HashMap<>();
        defines.put("flash", "true");
        target.setDefines(defines);
        Assert.assertEquals("The defined build constant 'flash' hasn't changed", "true", target.getDefines().get("flash"));

        // Check the task value
        project.evaluate();
        Swf swf = (Swf) project.getTasks().getByName("compileFlashSwf");
        Map<String, String> defs = swf.getDefines();
        Assert.assertEquals("The defined build constant 'swf' is not correct", "true", defs.get("swf"));
        Assert.assertEquals("The defined build constant 'flash' is not correct", "true", defs.get("flash"));
    }
}