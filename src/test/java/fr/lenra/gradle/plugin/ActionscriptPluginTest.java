package fr.lenra.gradle.plugin;

import static org.junit.Assert.assertNotNull;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Test;

public class ActionscriptPluginTest {

    @Test
    public void testApply() {
        Project project = ProjectBuilder.builder().build();
        project.getPluginManager().apply("fr.lenra.gradle.actionscript");
        assertNotNull(project.getExtensions().getByName("actionscript"));
    }
}