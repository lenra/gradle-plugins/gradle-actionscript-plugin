package fr.lenra.util;

import org.junit.Assert;
import org.junit.Test;

public class VersionComparatorTest {
	@Test
	public void testCompare() {
		String v1 = "1.0.0";
		String v2 = "2.0.0";
		VersionComparator c = new VersionComparator() {};
		Assert.assertEquals(-1, c.compare(v1, v2));
	}
}