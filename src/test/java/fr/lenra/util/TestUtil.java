package fr.lenra.util;

import org.gradle.api.Task;

public class TestUtil {
    public static void executeTask(Task t) {
        t.getActions().forEach(c -> c.execute(t));
    }
}