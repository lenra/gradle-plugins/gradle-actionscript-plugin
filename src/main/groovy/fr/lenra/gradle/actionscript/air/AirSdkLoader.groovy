package fr.lenra.gradle.actionscript.air

import org.gradle.api.internal.file.FileCollectionFactory;
import javax.inject.Inject;

class AirSdkLoader implements fr.lenra.gradle.actionscript.sdk.SdkLoader<AirSdk> {
	def parser = new XmlParser()
	def project;
	def fcf;

    @Inject
	AirSdkLoader(FileCollectionFactory fcf, org.gradle.api.Project project) {
		this.project = project;
		this.fcf = fcf;
	}
	
	def AirSdk load(location) {
		def sdkLocation = fcf.resolving(location).getSingleFile()
		def sdkDescription = parser.parse(new File(sdkLocation, 'air-sdk-description.xml'))
		def name = sdkDescription.name.text()
		def version = sdkDescription.version.text()
		def build = sdkDescription.build.text()
		def sdkXml = parser.parse(new File(sdkLocation, 'airsdk.xml'))
		def apps = new HashMap<String, Integer>()
		sdkXml.applicationNamespaces.versionMap.any { a ->
			apps.put(a.descriptorNamespace.text(), a.swfVersion.text())
		}
		def exts = new HashMap<String, Integer>()
		sdkXml.extensionNamespaces.versionMap.any { e ->
			exts.put(e.descriptorNamespace.text(), e.swfVersion.text())
		}
		return new AirSdk(project, name, version, build, sdkLocation, apps, exts)
	}
}