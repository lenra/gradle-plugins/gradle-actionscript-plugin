package fr.lenra.gradle.actionscript.air.application.descriptor

import groovy.xml.MarkupBuilder
import fr.lenra.gradle.actionscript.air.target.AirTarget
/**
* See https://help.adobe.com/en_US/air/build/WSfffb011ac560372f2fea1812938a6e463-8000.html and 
* https://help.adobe.com/fr_FR/air/build/WSfffb011ac560372f-5d0f4f25128cc9cd0cb-7ffe.html 
* to get all descriptor elements
*/
class AirAppDescriptor {
	def target

	AirAppDescriptor(AirTarget target) {
		this.target = target
	}

	def Closure<MarkupBuilder> applicationClosure() {
		return {MarkupBuilder applicationClosure -> }
	}

	def Closure<MarkupBuilder> initialWindowClosure() {
		return {MarkupBuilder initialWindow -> }
	}
}