package fr.lenra.gradle.actionscript.air

import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.Arrays
import java.util.HashMap

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import groovy.xml.MarkupBuilder
import groovy.xml.MarkupBuilderHelper
import fr.lenra.util.VersionComparator

class AirAppWriter {
	File swf
	File outputFile
	def write(project, String airNamespace, Closure<MarkupBuilder> applicationClosure, Closure<MarkupBuilder> initialWindowClosure) {
		def writer = new FileWriter(outputFile)
		def xml = new MarkupBuilder(writer)
		xml.doubleQuotes = true
		xml.mkp.xmlDeclaration(version: '1.0', encoding: 'utf-8')
		xml.application(xmlns: airNamespace) {
			id((project.group ? "${project.group}.":'') + project.name)
			versionNumber(project.version)
			filename(project.name)
			initialWindow {
				content(outputFile.parentFile.toPath().relativize(swf.toPath()))
				if (initialWindowClosure!=null)
					initialWindowClosure(xml)
			}
			if (applicationClosure!=null)
				applicationClosure(xml)
		}
		writer.close()
	}
}