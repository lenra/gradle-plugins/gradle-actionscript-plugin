package fr.lenra.gradle.actionscript.air.application.descriptor

import groovy.xml.MarkupBuilder
import fr.lenra.gradle.actionscript.air.target.MobileTarget

class MobileAppDescriptor extends AirAppDescriptor {
	MobileAppDescriptor(MobileTarget target) {
		super(target)
	}

	@Override
	def Closure<MarkupBuilder> initialWindowClosure() {
		return {MarkupBuilder initialWindow ->
			super.initialWindowClosure()(initialWindow)
			initialWindow.autoOrients('true')
			initialWindow.fullScreen('true')
			initialWindow.visible('true')
		}
	}
}