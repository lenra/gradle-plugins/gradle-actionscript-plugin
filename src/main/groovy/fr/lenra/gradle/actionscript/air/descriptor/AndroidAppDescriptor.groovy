package fr.lenra.gradle.actionscript.air.application.descriptor

import groovy.xml.MarkupBuilder
import fr.lenra.gradle.actionscript.air.target.AndroidTarget

class AndroidAppDescriptor extends MobileAppDescriptor {
	AndroidAppDescriptor(AndroidTarget target) {
		super(target)
	}

	@Override
	def Closure<MarkupBuilder> applicationClosure() {
		def str = new StringWriter()
		def xml = new MarkupBuilder(str)
		xml.manifest('android:installLocation': 'auto') {
			target.permissions.each { p ->
				'uses-permission'('android:name': "android.permission.$p")
			}
		}

		return {MarkupBuilder application ->
			super.applicationClosure()(application)
			application.android {
				manifestAdditions {
					application.mkp.yieldUnescaped '<![CDATA['+str.toString()+']]>'
				}
			}
		}
	}
}