package fr.lenra.util;

import java.util.Comparator;

public interface VersionComparator extends Comparator<String> {
	@Override
	default int compare(String o1, String o2) {
		String[] parts1 = o1.split("[._]");
		String[] parts2 = o2.split("[._]");
		for (int i = 0; i < parts1.length && i < parts2.length; i++) {
			String val1 = parts1[i];
			String val2 = parts2[i];
			val1 = val1.replaceAll("^[a-zA-Z]?(\\d+)([^0-9].*)?$", "$1");
			val2 = val2.replaceAll("^[a-zA-Z]?(\\d+)([^0-9].*)?$", "$1");
			int c = Integer.compare(Integer.parseInt(val1), Integer.parseInt(val2));
			if (c!=0)
				return c;
		}
		return Integer.compare(parts1.length, parts2.length);
	}
}