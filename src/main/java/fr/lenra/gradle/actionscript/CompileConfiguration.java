package fr.lenra.gradle.actionscript;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.gradle.api.Project;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

public class CompileConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final transient Project project;
	private final CompileConfiguration parentConfiguration;

	private final transient Property<String> mainClass;
	private final transient Property<String> embedDependenciesPath;
	private final transient Provider<Map<String, String>> defines;
	private final transient Map<String, String> definesMap = new HashMap<>();
	
	private final transient Property<String> targetPlayer;
	private final transient Property<Integer> swfVersion;

	public CompileConfiguration(Project project) {
		this(project, null);
	}

	protected CompileConfiguration(CompileConfiguration parentConfiguration) {
		this(parentConfiguration.getProject(), parentConfiguration);
	}

	protected CompileConfiguration(Project project, CompileConfiguration parentConfiguration) {
		this.project = project;
		this.parentConfiguration = parentConfiguration;
		this.mainClass = project.getObjects().property(String.class);
		this.embedDependenciesPath = project.getObjects().property(String.class);
		this.targetPlayer = project.getObjects().property(String.class);
		this.swfVersion = project.getObjects().property(Integer.class);

		if (parentConfiguration!=null) {
			mainClass.convention(parentConfiguration.mainClass);
			embedDependenciesPath.convention(parentConfiguration.embedDependenciesPath);
			defines = parentConfiguration.defines.map(m2 -> {
				HashMap<String, String> ret = new HashMap<>(m2);
				ret.putAll(definesMap);
				return ret;
			});
			targetPlayer.convention(parentConfiguration.targetPlayer);
			swfVersion.convention(parentConfiguration.swfVersion);
		}
		else {
			defines = project.getProviders().provider(() -> definesMap);
		}
	}

	public Project getProject() {
		return this.project;
	}

	public CompileConfiguration getParentConfiguration() {
		return parentConfiguration;
	}

	public String getMainClass() {
		return mainClass.getOrNull();
	}

	public void setMainClass(String mainClass) {
		this.mainClass.set(mainClass);
	}

	public String getEmbedDependenciesPath() {
		return embedDependenciesPath.getOrNull();
	}

	public void setEmbedDependenciesPath(String embedDependenciesPath) {
		this.embedDependenciesPath.set(embedDependenciesPath);
	}

	public Path getMainClassPath(Set<File> srcDirs) {
		String mainClass = getMainClass();
		if (mainClass==null)
			return null;

		final String mainFile = mainClass.replaceAll("[.]", "/") + ".as";
		return srcDirs
			.stream()
			.map(dir -> dir.toPath().resolve(mainFile))
			.filter(p -> p.toFile().exists())
			.findFirst()
			.orElse(null);
	}

	public Map<String, String> getDefines() {
		return defines.get();
	}

	public void setDefines(Map<String, String> defines) {
		this.definesMap.putAll(defines);
	}

	public String getTargetPlayer() {
		return targetPlayer.getOrNull();
	}

	public void setTargetPlayer(String targetPlayer) {
		this.targetPlayer.set(targetPlayer);
	}

	public Integer getSwfVersion() {
		return swfVersion.getOrNull();
	}

	public void setSwfVersion(Integer swfVersion) {
		this.swfVersion.set(swfVersion);
	}
}