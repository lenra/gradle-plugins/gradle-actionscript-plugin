package fr.lenra.gradle.actionscript.sdk;

import org.gradle.api.NamedDomainObjectContainer;

public interface SdkContainer extends NamedDomainObjectContainer<Sdk> {
	void addLoader(String name, SdkLoader<?> loader);
}