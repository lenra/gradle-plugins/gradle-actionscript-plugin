package fr.lenra.gradle.actionscript.sdk;

public interface SdkLoader<T extends Sdk> {
	T load(Object location);
}