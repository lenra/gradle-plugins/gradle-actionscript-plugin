package fr.lenra.gradle.actionscript.sdk.internal;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.gradle.api.Namer;
import org.gradle.api.Project;
import org.gradle.api.internal.AbstractValidatingNamedDomainObjectContainer;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.internal.reflect.Instantiator;

import fr.lenra.gradle.actionscript.sdk.Sdk;
import fr.lenra.gradle.actionscript.sdk.SdkContainer;
import fr.lenra.gradle.actionscript.sdk.SdkLoader;

public class DefaultSdkContainer extends AbstractValidatingNamedDomainObjectContainer<Sdk> implements SdkContainer {

	private Project project;
	private Map<String, SdkLoader<?>> loaders = new HashMap<>();

	@Inject
	public DefaultSdkContainer(Instantiator instantiator,
			CollectionCallbackActionDecorator collectionCallbackActionDecorator, Project project) throws ClassNotFoundException {
		super(Sdk.class, instantiator, new SdkNamer(), collectionCallbackActionDecorator);
		this.project = project;
	}

	@Override
	protected Sdk doCreate(String name) {
		return doCreate(name, name, null);
	}

	protected Sdk doCreate(String name, String loaderName, Object location) {
		SdkLoader<?> loader = loaders.get(loaderName);
		if (location==null) {
			String path = System.getenv().get(name.toUpperCase()+"_HOME");
			location = new File(path);
		}
		Sdk ret = loader.load(location);
		((SdkNamer)getNamer()).set(name, ret);
		return ret;
	}

	public Project getProject() {
		return this.project;
	}

	public void addLoader(String name, SdkLoader<?> loader) {
		this.loaders.put(name, loader);
	}

	private static class SdkNamer implements Namer<Sdk> {
		private final Map<String, Sdk> sdks = new HashMap<>();

		@Override
		public String determineName(Sdk sdk) {
			return sdks.entrySet()
				.stream()
				.filter(e -> e.getValue()==sdk)
				.map(e -> e.getKey())
				.findFirst()
				.orElse(null);
		}

		public void set(String name, Sdk sdk) {
			sdks.put(name, sdk);
		}
	}
}