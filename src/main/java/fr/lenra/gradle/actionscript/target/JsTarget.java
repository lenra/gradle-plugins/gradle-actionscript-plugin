package fr.lenra.gradle.actionscript.target;

// import java.io.BufferedOutputStream;
// import java.io.File;
// import java.io.FileNotFoundException;
// import java.io.FileOutputStream;
// import java.io.IOException;
// import java.nio.file.Path;
// import java.util.ArrayList;
// import java.util.List;
// import java.util.Set;
// import java.util.TreeSet;
// import java.util.stream.Collectors;

// import javax.inject.Inject;

// import org.apache.royale.compiler.Messages;
// import org.apache.royale.compiler.clients.problems.ProblemQuery;
// import org.apache.royale.compiler.codegen.js.IJSPublisher;
// import org.apache.royale.compiler.codegen.js.IJSWriter;
// import org.apache.royale.compiler.config.Configuration;
// import org.apache.royale.compiler.config.Configurator;
// import org.apache.royale.compiler.driver.js.IJSApplication;
// import org.apache.royale.compiler.driver.js.IJSBackend;
// import org.apache.royale.compiler.internal.driver.mxml.jsc.MXMLJSCJSBackend;
// import org.apache.royale.compiler.internal.projects.RoyaleJSProject;
// import org.apache.royale.compiler.internal.targets.JSTarget;
// import org.apache.royale.compiler.internal.targets.RoyaleJSTarget;
// import org.apache.royale.compiler.internal.workspaces.Workspace;
// import org.apache.royale.compiler.problems.ICompilerProblem;
// import org.apache.royale.compiler.projects.ICompilerProject;
// import org.apache.royale.compiler.targets.ITargetSettings;
// import org.apache.royale.compiler.units.ICompilationUnit;
// import org.gradle.api.Project;

// import fr.lenra.gradle.actionscript.CompileConfiguration;
// import fr.lenra.gradle.task.ActionScriptCompile;

// @Target("js")
// public class JsTarget extends AbstractTargetCompiler implements PackageableTarget {
// 	private IJSBackend backend;

// 	@Inject
// 	public JsTarget(Project project, CompileConfiguration parentConfiguration) {
// 		super(project, parentConfiguration);
// 		this.backend = this.createBackend();
// 	}

// 	@Override
// 	protected ICompilerProject createCompilerProject() {
// 		return new RoyaleJSProject(new Workspace(), backend);
// 	}

// 	@Override
// 	protected Configurator createConfigurator() {
// 		return backend.createConfigurator();
// 	}

// 	protected IJSBackend createBackend() {
// 		return new MXMLJSCJSBackend();
// 	}

// 	@Override
// 	public void configure() {
// 		ActionScriptCompile compileTask = this.getCompileTask();
// 		RoyaleJSProject project = (RoyaleJSProject) this.getCompilerProject();
// 		Configurator configurator = getConfigurator();

// 		configurator.setOutput(this.getOutputDirectoryPath().toFile());
// 		configurator.enableAccessibility(true);
// 		configurator.optimize(true);
// 		configurator.setExcludeNativeJSLibraries(true);
// 		configurator.allowSourcePathOverlap(false);

// 		configurator.enableDebugging(true, null);

// 		configurator.setDefineDirectives(this.getDefines());

// 		// TODO: If a main class is defined, only include this main file
// 		if (getMainClassPath()!= null) {
// 			Set<File> srcDirs = compileTask.getLanguageSourceSet().getByLanguage(compileTask.getLanguage())
// 					.getSrcDirs();
// 			configurator.addSourcePath(srcDirs);
// 			configurator.setMainDefinition(this.getMainClass());
// 			Path path = this.getMainClassPath();
// 			configurator.setConfiguration(new String[] { path.toString() }, "file-specs");
// 		} else
// 			configurator.addIncludes(
// 					compileTask.getSource().getFiles().stream().map(f -> f.getPath()).collect(Collectors.toList()));
// 		configurator.applyToProject(project);
// 		configurator.addExternalLibraryPath(this.getCompileClasspath().getFiles());
// 		configurator.applyToProject(project);
// 		configurator.useNetwork(true);
// 	}

// 	@Override
// 	public void compile() throws InterruptedException, FileNotFoundException, IOException {
// 		RoyaleJSProject project = (RoyaleJSProject) this.getCompilerProject();
// 		Configurator configurator = getConfigurator();
// 		ProblemQuery problems = getProblems();

// 		this.getProject().getLogger().debug("Create target settings");
// 		ITargetSettings targetSettings = configurator.getTargetSettings(null);

// 		this.getProject().getLogger().debug("Get configuration");
// 		Configuration config = configurator.getConfiguration();
// 		Messages.setLocale(config.getToolsLocale());
// 		project.apiReportFile = config.getApiReport();

// 		problems.addAll(project.getFatalProblems());

// 		this.getProject().getLogger().debug("Create target");

// 		JSTarget target;
// 		target = new JSTarget(project, targetSettings, null);

// 		Path path = getMainClassPath();
// 		ICompilationUnit mainCU = null;
// 		if (path != null) 
// 			mainCU = project.getWorkspace().getCompilationUnits(path.toString(), project).stream().findFirst().orElseGet(null);

// 		List<ICompilerProblem> buildingProblems = new ArrayList<ICompilerProblem>();
// 		final IJSApplication artifact = target.build(mainCU, buildingProblems);
// 		problems.addAll(buildingProblems);

// 		List<ICompilerProblem> errors = new ArrayList<>();
// 		IJSPublisher jsPublisher = (IJSPublisher) backend.createPublisher(project, errors, config);

// 		File outputFolder = jsPublisher.getOutputFolder();

// 		ArrayList<ICompilationUnit> roots = new ArrayList<ICompilationUnit>();
// 		roots.add(mainCU);
// 		Set<ICompilationUnit> incs = target.getIncludesCompilationUnits();
// 		roots.addAll(incs);
// 		project.mixinClassNames = new TreeSet<String>();
// 		List<ICompilationUnit> reachableCompilationUnits = project.getReachableCompilationUnitsInSWFOrder(roots);
// 		((RoyaleJSTarget)target).collectMixinMetaData(project.mixinClassNames, reachableCompilationUnits);
// 		for (final ICompilationUnit cu : reachableCompilationUnits)
// 		{
// 			ICompilationUnit.UnitType cuType = cu.getCompilationUnitType();

// 			if (cuType == ICompilationUnit.UnitType.AS_UNIT
// 					|| cuType == ICompilationUnit.UnitType.MXML_UNIT)
// 			{
// 				final File outputClassFile = getOutputClassFile(
// 						cu.getQualifiedNames().get(0), outputFolder);

// 				System.out.println("Compiling file: " + outputClassFile);

// 				ICompilationUnit unit = cu;

// 				IJSWriter writer;
// 				if (cuType == ICompilationUnit.UnitType.AS_UNIT)
// 				{
// 					writer = (IJSWriter) project.getBackend().createWriter(project,
// 							errors, unit, false);
// 				}
// 				else
// 				{
// 					writer = (IJSWriter) project.getBackend().createMXMLWriter(
// 							project, errors, unit, false);
// 				}

// 				BufferedOutputStream out = new BufferedOutputStream(
// 						new FileOutputStream(outputClassFile));

// 				BufferedOutputStream sourceMapOut = null;
// 				File outputSourceMapFile = null;
// 				if (project.config.getSourceMap())
// 				{
// 					outputSourceMapFile = getOutputSourceMapFile(
// 							cu.getQualifiedNames().get(0), outputFolder);
// 					sourceMapOut = new BufferedOutputStream(
// 							new FileOutputStream(outputSourceMapFile));
// 				}
				
// 				writer.writeTo(out, sourceMapOut, outputSourceMapFile);
// 				out.flush();
// 				out.close();
// 				if (sourceMapOut != null)
// 				{
// 					sourceMapOut.flush();
// 					sourceMapOut.close();
// 				}
// 				writer.close();
// 			}
// 		}

// 		jsPublisher.publish(problems);
// 	}
	

// 	// TODO: reimplement

//     /**
//      * @author Erik de Bruin
//      * 
//      *         Get the output class file. This includes the (sub)directory in
//      *         which the original class file lives. If the directory structure
//      *         doesn't exist, it is created.
//      * 
//      * @param qname
//      * @param outputFolder
//      * @return output class file path
//      */
//     private File getOutputClassFile(String qname, File outputFolder)
//     {
//         String[] cname = qname.split("\\.");
//         String sdirPath = outputFolder + File.separator;
//         if (cname.length > 0)
//         {
//             for (int i = 0, n = cname.length - 1; i < n; i++)
//             {
//                 sdirPath += cname[i] + File.separator;
//             }

//             File sdir = new File(sdirPath);
//             if (!sdir.exists())
//                 sdir.mkdirs();

//             qname = cname[cname.length - 1];
//         }

//         return new File(sdirPath + qname + "." + backend.getOutputExtension());
//     }

//     /**
//      * @param qname
//      * @param outputFolder
//      * @return output source map file path
//      */
//     private File getOutputSourceMapFile(String qname, File outputFolder)
//     {
//         String[] cname = qname.split("\\.");
//         String sdirPath = outputFolder + File.separator;
//         if (cname.length > 0)
//         {
//             for (int i = 0, n = cname.length - 1; i < n; i++)
//             {
//                 sdirPath += cname[i] + File.separator;
//             }

//             File sdir = new File(sdirPath);
//             if (!sdir.exists())
//                 sdir.mkdirs();

//             qname = cname[cname.length - 1];
//         }

//         return new File(sdirPath + qname + "." + backend.getOutputExtension() + ".map");
//     }
// }