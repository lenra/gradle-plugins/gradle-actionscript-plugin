package fr.lenra.gradle.actionscript.target;

import java.io.File;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.plugins.BasePlugin;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.sourceset.LanguageSourceSet;
import fr.lenra.gradle.task.AbstractActionScriptArchiveTask;
import fr.lenra.gradle.task.Swc;
import fr.lenra.gradle.task.Swf;

@Target(value = "swf", abstracted = true)
public class SwfTarget extends AbstractTargetCompiler implements PackageableTarget {
	private static final long serialVersionUID = 1L;

	@Inject
	public SwfTarget(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
	}

	@Override
	public TaskProvider<? extends AbstractArchiveTask> createArchiveTask(LanguageSourceSet sourceSet, boolean debug,
			Configuration compileClasspath, Configuration runtimeClasspath, Configuration embedConfiguration) {
		String name = this.getName();
		String sourceSetName = sourceSet.getName();
		Class<? extends AbstractActionScriptArchiveTask> c = isLibrary() ? Swc.class : Swf.class;
		this.getProject().getLogger().info("Create target {} {} task for sourceSet {}", name, c.getSimpleName(),
				sourceSetName);

		String taskName = generateTaskName("copy", sourceSet, "sources");
		File tmpSourceDir = sourceSet.getAt("actionscript").getOutputDir().toPath().resolve(name).toFile();
		TaskProvider<Copy> copy = this.getProject().getTasks().register(taskName, Copy.class, task -> {
			task.from(sourceSet.getAt("actionscript"));
			task.from(sourceSet.getResources());
			task.from(embedConfiguration, cp -> {
				String embedPath = this.getEmbedDependenciesPath();
				if (embedPath != null)
					cp.into(embedPath);
				cp.rename(
						f -> f.replaceAll("^(((?!-\\d+\\.\\d+\\.\\d+).)+)(-\\d+\\.\\d+\\.\\d+.*)?(\\.[^.]+)$", "$1$4"));
			});
			task.into(tmpSourceDir);
		});

		SourceDirectorySet dirSet = getProject().getObjects().sourceDirectorySet("allSource", "allSource");
		dirSet.getFilter().include("**/*.as");
		// TODO: add mxml (genericly)
		dirSet.srcDir(tmpSourceDir);

		taskName = generateTaskName("compile", sourceSet, c.getSimpleName());
		return this.getProject().getTasks().register(taskName, c, task -> {
			task.dependsOn(copy);
			task.setDefines(getDefines());
			task.getArchiveClassifier().set(getClassifierName(sourceSet));
			task.setDebug(debug);
			task.setTargetPlayer(this.getTargetPlayer());
			task.setSwfVersion(this.getSwfVersion());
			task.setSourceDirs(dirSet);
			task.setCompileClasspath(compileClasspath);
			task.setRuntimeClasspath(runtimeClasspath);
			task.setGroup(BasePlugin.BUILD_GROUP);
			task.setDescription("Assembles "+sourceSetName+" SWF for language ActionScript and target "+name+".");
			if (!isLibrary()) {
				((Swf)task).setMainClass(this.getMainClass());
			}
		});
	}
}