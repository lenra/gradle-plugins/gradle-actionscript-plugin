package fr.lenra.gradle.actionscript.target;

import org.gradle.api.NamedDomainObjectContainer;

public interface TargetContainer extends NamedDomainObjectContainer<AbstractTargetCompiler> {
	
}