package fr.lenra.gradle.actionscript.target;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;
import org.gradle.util.GUtil;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.plugin.ActionScriptExtension;
import fr.lenra.gradle.sourceset.LanguageSourceSet;

public abstract class AbstractTargetCompiler extends CompileConfiguration {
	private static final long serialVersionUID = 1L;
	
	private Boolean abstracted;

	@Inject
	public AbstractTargetCompiler(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
	}

	public final String getName() {
		return this.getClass().getAnnotation(Target.class).value();
	}

	public boolean isLibrary() {
		return getProject().getExtensions().getByType(ActionScriptExtension.class).isLibrary();
	}

	public boolean getIsAbstract() {
		if (abstracted!=null)
			return abstracted;
		return this.getClass().getAnnotation(Target.class).abstracted();
	}

	public void setIsAbstract(boolean a) {
		abstracted = a;
	}

	protected final String generateTaskName(String action, LanguageSourceSet sourceSet, String... suffixes) {
		String name = this.getName();
		String sourceSetName = sourceSet.getName();
		if (!sourceSetName.equals(SourceSet.MAIN_SOURCE_SET_NAME))
			name = sourceSetName + " " + name;
		return GUtil.toLowerCamelCase(action + " " + name + " " + String.join(" ", suffixes));
	}

	protected final String getClassifierName(LanguageSourceSet sourceSet) {
		List<String> parts = new ArrayList<>();
		String name = sourceSet.getName();
		if (!name.equals(SourceSet.MAIN_SOURCE_SET_NAME))
			parts.add(name);
		name = this.getName();
		if (!"swf".equals(name))
			parts.add(name);
		if (parts.isEmpty())
			return "";
		return parts.stream().collect(Collectors.joining("-"));
	}

	public abstract TaskProvider<? extends AbstractArchiveTask> createArchiveTask(LanguageSourceSet sourceSet, boolean debug,
			Configuration compileClasspath, Configuration runtimeClasspath, Configuration embedConfiguration);

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AbstractTargetCompiler && super.equals(obj)) {
			return this.getIsAbstract()==((AbstractTargetCompiler) obj).getIsAbstract();
		}
		return false;
	}
}