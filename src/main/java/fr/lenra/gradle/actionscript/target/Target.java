package fr.lenra.gradle.actionscript.target;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.gradle.api.attributes.Attribute;

@java.lang.annotation.Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Target {
	public static final Attribute<String> TARGET_ATTRIBUTE = Attribute.of("actionscript.target", String.class);

	String value();

	// TODO: change to array
	String extend() default "";

	boolean abstracted() default false;
}