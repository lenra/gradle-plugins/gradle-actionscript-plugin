package fr.lenra.gradle.actionscript.target.internal;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.gradle.api.Namer;
import org.gradle.api.Project;
import org.gradle.api.internal.AbstractValidatingNamedDomainObjectContainer;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.internal.reflect.Instantiator;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.target.AbstractTargetCompiler;
import fr.lenra.gradle.actionscript.target.Target;
import fr.lenra.gradle.actionscript.target.TargetContainer;
import fr.lenra.gradle.plugin.ActionScriptExtension;

public class DefaultTargetContainer extends AbstractValidatingNamedDomainObjectContainer<AbstractTargetCompiler>
		implements TargetContainer {

	private Project project;
	private CompileConfiguration configuration;
	private final Map<String, Class<AbstractTargetCompiler>> availableTargets = new HashMap<>();
	private final List<String> searchedPackage = new ArrayList<>();
	private final Map<String, String> targetParents = new HashMap<>();
	private final ActionScriptExtension extension;
	private final ClassPathScanningCandidateComponentProvider scanner;

	@Inject
	public DefaultTargetContainer(Instantiator instantiator,
			CollectionCallbackActionDecorator collectionCallbackActionDecorator, Project project,
			ActionScriptExtension extension, CompileConfiguration configuration) throws ClassNotFoundException {
		super(AbstractTargetCompiler.class, instantiator,
				(Namer<? super AbstractTargetCompiler>) new Namer<AbstractTargetCompiler>() {
					public String determineName(AbstractTargetCompiler ss) {
						return ss.getName();
					}
				}, collectionCallbackActionDecorator);
		this.project = project;
		this.configuration = configuration;
		this.extension = extension;
		this.scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AssignableTypeFilter(AbstractTargetCompiler.class));
		scanner.addIncludeFilter(new AnnotationTypeFilter(Target.class));
	}

	@Override
	protected AbstractTargetCompiler doCreate(String name) {
		// Update the target list first
		updateSearchedPackages();

		// Get the parent target
		String parentName = targetParents.get(name);
		CompileConfiguration parent = (parentName != null) ? this.maybeCreate(parentName) : getConfiguration();

		// Create the target
		Class<AbstractTargetCompiler> c = getTargetClass(name);
		try {
			return c.getConstructor(Project.class, CompileConfiguration.class).newInstance(getProject(), parent);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public CompileConfiguration getConfiguration() {
		return this.configuration;
	}

	public Project getProject() {
		return this.project;
	}

	private Class<AbstractTargetCompiler> getTargetClass(String name) {
		return availableTargets.get(name);
	}

	@SuppressWarnings("unchecked")
	private void updateSearchedPackages() {
		ClassLoader classLoader = DefaultTargetContainer.class.getClassLoader();
		extension.getTargetPackages()
			.stream()
			.filter(p -> !searchedPackage.contains(p))
			.forEach(p -> {
				searchedPackage.add(p);
				scanner.findCandidateComponents(p).stream().forEach(bd -> {
					try {
						Class<AbstractTargetCompiler> c = (Class<AbstractTargetCompiler>) classLoader
								.loadClass(bd.getBeanClassName());
						Target target = c.getAnnotation(Target.class);
						String name = target.value();
						String parent = target.extend();
						if (StringUtils.isNotBlank(parent))
							targetParents.put(name, parent);
						if (availableTargets.containsKey(name)) {
							project.getLogger().warn("Target name duplication: {}", name);
							return;
						}
						project.getLogger().debug("Add the target {} with class {}", name, bd.getBeanClassName());
						availableTargets.put(name, c);
					} catch (ClassNotFoundException e) {
						project.getLogger().warn("Target class not found: {}", bd.getBeanClassName());
					}
				});
			});
	}
}