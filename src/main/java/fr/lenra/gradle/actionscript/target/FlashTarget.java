package fr.lenra.gradle.actionscript.target;

import javax.inject.Inject;

import org.gradle.api.Project;

import fr.lenra.gradle.actionscript.CompileConfiguration;

@Target(value = "flash", extend = "swf")
public class FlashTarget extends SwfTarget {
	private static final long serialVersionUID = 1L;
	
	@Inject
	public FlashTarget(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
	}
}