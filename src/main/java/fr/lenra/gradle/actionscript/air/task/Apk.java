package fr.lenra.gradle.actionscript.air.task;

import fr.lenra.gradle.actionscript.air.tool.adt.AndroidTarget;
import fr.lenra.gradle.actionscript.air.tool.adt.Target;

public class Apk extends AbstractAirPackageTask {

	public Apk() {
		this.getArchiveExtension().set("apk");
		this.setTarget(AndroidTarget.APK);
	}

	@Override
	protected Class<? extends Target> getTargetClass() {
		return AndroidTarget.class;
	}
}