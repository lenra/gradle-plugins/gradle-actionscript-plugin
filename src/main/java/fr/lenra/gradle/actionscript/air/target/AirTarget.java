package fr.lenra.gradle.actionscript.air.target;

import java.io.File;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.RegularFile;
import org.gradle.api.plugins.BasePlugin;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.air.application.descriptor.AirAppDescriptor;
import fr.lenra.gradle.actionscript.air.task.AbstractAirPackageTask;
import fr.lenra.gradle.actionscript.air.task.Adl;
import fr.lenra.gradle.actionscript.air.task.Air;
import fr.lenra.gradle.actionscript.air.task.Airi;
import fr.lenra.gradle.actionscript.air.task.GenerateAirAppDescriptor;
import fr.lenra.gradle.actionscript.target.SwfTarget;
import fr.lenra.gradle.actionscript.target.Target;
import fr.lenra.gradle.sourceset.LanguageSourceSet;

@Target(value = "air", extend = "swf", abstracted = true)
public class AirTarget extends SwfTarget {
	private static final long serialVersionUID = 1L;
	
	private transient AirAppDescriptor appDescriptor;
	private final transient Property<Object> keystore;
	private final transient Property<Boolean> packageAirArtifact;
	protected transient TaskProvider<Adl> adl;
	protected transient Airi prepareTask;
	// private List<?> extensions;

	@Inject
	public AirTarget(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
		this.keystore = project.getObjects().property(Object.class);
		this.packageAirArtifact = project.getObjects().property(Boolean.class);
		this.packageAirArtifact.convention(Boolean.TRUE);
		if (parentConfiguration!=null && parentConfiguration instanceof AirTarget) {
			keystore.convention(((AirTarget)parentConfiguration).keystore);
			packageAirArtifact.convention(((AirTarget)parentConfiguration).packageAirArtifact);
		}
	}

	public AirAppDescriptor getAppDescriptor() {
		if (appDescriptor==null)
			appDescriptor = this.createAppDescriptor();
		return appDescriptor;
	}

	protected AirAppDescriptor createAppDescriptor() {
		return new AirAppDescriptor(this);
	}

	public Object getKeystore() {
		return keystore.get();
	}

	public void setKeystore(Object keystore) {
		this.keystore.set(keystore);
	}

	/**
	 * @return the packageAirArtifact
	 */
	public boolean isPackageAirArtifact() {
		return packageAirArtifact.get();
	}

	/**
	 * @param packageAirArtifact the packageAirArtifact to set
	 */
	public void setPackageAirArtifact(Boolean packageAirArtifact) {
		if (packageAirArtifact!=null)
			this.packageAirArtifact.set(packageAirArtifact);
	}

	protected Class<? extends AbstractAirPackageTask> getPackageTaskClass() {
		return Air.class;
	}

	@Override
	public TaskProvider<? extends AbstractArchiveTask> createArchiveTask(LanguageSourceSet sourceSet, boolean debug,
			Configuration compileClasspath, Configuration runtimeClasspath, Configuration embedConfiguration) {
		TaskProvider<? extends AbstractArchiveTask> provider = super.createArchiveTask(sourceSet, debug,
				compileClasspath, runtimeClasspath, embedConfiguration);

		if (isPackageAirArtifact()) {
			// TODO: if library package ANE
			if (!isLibrary()) {
				String name = this.getName();
				String sourceSetName = sourceSet.getName();
				provider.configure(t -> {
					t.setDestinationDir(getProject().getBuildDir().toPath().resolve("air/"+name).toFile());
				});

				this.getProject().getLogger().info("Create AIR app descriptor task for {} target and {} sourceSet", name, sourceSetName);
				String taskName = generateTaskName("generate", sourceSet, "app", "descriptor");
				TaskProvider<GenerateAirAppDescriptor> descriptorTask = getProject().getTasks().register(taskName, GenerateAirAppDescriptor.class, t -> {
					t.setTarget(this);
					t.setGroup(BasePlugin.BUILD_GROUP);
					t.setDescription("Create the " + sourceSetName + " application descriptor xml file for target " + name + ".");
				
					AbstractArchiveTask swfTask = provider.get();
					Provider<RegularFile> swfFile = swfTask.getArchiveFile();
					t.setSwfProvider(swfFile);
					t.setOutputFileProvider(swfFile
						.map(f -> new File(f.getAsFile().getParentFile(), getProject().getName()+"-app-"+swfTask.getArchiveClassifier().get()+".xml")));
					t.dependsOn(provider);
				});

				if (debug) {
					taskName = generateTaskName("", sourceSet, "application");
					this.adl = getProject().getTasks().register(taskName, Adl.class, t -> {
						t.setGroup(BasePlugin.BUILD_GROUP);
						t.descriptor(descriptorTask.get().getOutputs());
						t.dependsOn(descriptorTask);
					});
				}

				taskName = generateTaskName("prepare", sourceSet, "application");
				TaskProvider<Airi> airi = getProject().getTasks().register(taskName, Airi.class, t -> {
					AbstractArchiveTask swfTask = provider.get();
					t.getArchiveClassifier().set(swfTask.getArchiveClassifier().get());
					t.setGroup(BasePlugin.BUILD_GROUP);
					t.descriptor(descriptorTask.get().getOutputs());
					t.swf(swfTask.getOutputs());
					t.dependsOn(descriptorTask, provider);
				});

				taskName = generateTaskName("package", sourceSet, "application");
				TaskProvider<? extends AbstractAirPackageTask> pack = getProject().getTasks().register(taskName,
						getPackageTaskClass(), t -> {
					t.getArchiveClassifier().set(provider.get().getArchiveClassifier().get());
					t.setGroup(BasePlugin.BUILD_GROUP);
					t.descriptor(airi.get().getArchiveFile());
					t.keystore(keystore);
					t.dependsOn(airi);
				});
				return pack;
			}
		}
		return provider;
	}
}