package fr.lenra.gradle.actionscript.air.task;

import java.io.File;
import java.util.Map.Entry;

import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFile;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import fr.lenra.gradle.actionscript.air.AirAppWriter;
import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.actionscript.air.application.descriptor.AirAppDescriptor;
import fr.lenra.gradle.actionscript.air.target.AirTarget;
import fr.lenra.gradle.plugin.ActionScriptExtension;
import fr.lenra.util.VersionComparator;

public class GenerateAirAppDescriptor extends DefaultTask {
	private AirTarget target;
	private Provider<RegularFile> swf;
	private Provider<File> outputFile;

	/**
	 * @return the target
	 */
	@Input
	public AirTarget getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(AirTarget target) {
		this.target = target;
	}

	/**
	 * @return the swf
	 */
	@InputFile
	public File getSwf() {
		return swf.get().getAsFile();
	}

	/**
	 * @param swf the swf to set
	 */
	public void setSwfProvider(Provider<RegularFile> swf) {
		this.swf = swf;
	}

	/**
	 * @return the outputFile
	 */
	@OutputFile
	public File getOutputFile() {
		return outputFile.get();
	}

	/**
	 * @param outputFile the outputFile to set
	 */
	public void setOutputFileProvider(Provider<File> outputFile) {
		this.outputFile = outputFile;
	}

	@TaskAction
	public void createAppDescriptor() {
		AirSdk sdk = (AirSdk) ((ActionScriptExtension) getProject().getExtensions().getByName("actionscript")).getSdks().getAt("air");
		Entry<String, String> airVersion = sdk.getApplicationNamespaces()
			.entrySet()
			.stream()
			.max((a, b) -> new VersionComparator(){}.compare(a.getKey(), b.getKey()))
			.orElse(null);
		AirAppWriter writer = new AirAppWriter();
		writer.setSwf(this.swf.get().getAsFile());
		writer.setOutputFile(this.outputFile.get());
		AirAppDescriptor descriptor = target.getAppDescriptor();
		writer.write(getProject(), airVersion.getValue(), descriptor.applicationClosure(), descriptor.initialWindowClosure());
	}
}