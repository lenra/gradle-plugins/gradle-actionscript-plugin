package fr.lenra.gradle.actionscript.air.tool.adt;

public interface Target {
	String name();
	default String getValue() {
		return this.name().toLowerCase().replaceAll("_", "-");
	}
}