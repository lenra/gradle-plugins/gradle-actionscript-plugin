package fr.lenra.gradle.actionscript.air.task;

import fr.lenra.gradle.actionscript.air.tool.adt.AirTarget;
import fr.lenra.gradle.actionscript.air.tool.adt.Target;

public class Air extends AbstractAirPackageTask {

	public Air() {
		this.getArchiveExtension().set("air");
	}

	@Override
	protected Class<? extends Target> getTargetClass() {
		return AirTarget.class;
	}
}