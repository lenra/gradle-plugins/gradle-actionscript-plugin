package fr.lenra.gradle.actionscript.air.plugin;

import java.util.Map;

import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.plugins.ExtensionAware;

import fr.lenra.gradle.actionscript.air.task.DescribeSdkTask;
import fr.lenra.gradle.actionscript.sdk.Sdk;
import fr.lenra.gradle.actionscript.sdk.SdkLoader;
import fr.lenra.gradle.plugin.ActionScriptExtension;
import fr.lenra.gradle.plugin.language.LanguagePlugin;
import fr.lenra.gradle.actionscript.air.AirSdkLoader;

public class ActionScriptAirPlugin extends LanguagePlugin {

	@Override
	public void apply(ProjectInternal project) {
		super.apply(project);
		project.getPluginManager().apply("fr.lenra.gradle.actionscript");

		if (!project.getExtensions().getExtraProperties().has("fr.lenra.gradle.actionscript-air.applied")) {
			project.getExtensions().getExtraProperties().set("fr.lenra.gradle.actionscript-air.applied", true);
			ActionScriptExtension extension = project.getExtensions().getByType(ActionScriptExtension.class);
			extension.getTargetPackages().add("fr.lenra.gradle.actionscript.air.target");
			extension.getSdks().addLoader("air",
					(SdkLoader<?>) project.getObjects().newInstance(AirSdkLoader.class, project));
			Sdk airSdk = extension.getSdks().create("air");
			// Deprecated
			((ExtensionAware)extension).getExtensions().add("airSdk", airSdk);
			
			// TODO: manage the discribe SDK tasks by the SDK container
			project.getTasks().register("describeAirSdk", DescribeSdkTask.class, d -> d.setSdk(airSdk));
		}
	}
}