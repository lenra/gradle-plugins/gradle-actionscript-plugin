package fr.lenra.gradle.actionscript.air.task;

import java.io.IOException;

import org.gradle.api.Project;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.diagnostics.AbstractReportTask;
import org.gradle.api.tasks.diagnostics.internal.ReportRenderer;
import org.gradle.api.tasks.diagnostics.internal.TextReportRenderer;

import fr.lenra.gradle.actionscript.sdk.Sdk;

public class DescribeSdkTask extends AbstractReportTask {
	private TextReportRenderer renderer = new TextReportRenderer();

	@Input
	private Sdk sdk;

	/**
	 * @return the sdk
	 */
	public Sdk getSdk() {
		return sdk;
	}

	/**
	 * @param sdk the sdk to set
	 */
	public void setSdk(Sdk sdk) {
		this.sdk = sdk;
	}

	@Override
	protected void generate(Project project) throws IOException {
		if (sdk==null)
			// TODO: display the download address or create a task to download it
			throw new NullPointerException("No SDK is defined");
		renderer.getTextOutput().println(sdk);
	}

	@Override
	protected ReportRenderer getRenderer() {
		return renderer;
	}
}