package fr.lenra.gradle.actionscript.air.tool.adt;

public enum AirTarget implements Target {
	AIR, NATIVE, ANE
}