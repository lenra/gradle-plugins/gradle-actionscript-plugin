package fr.lenra.gradle.actionscript.air.task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.gradle.api.internal.file.FileCollectionFactory;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

public abstract class AbstractAirArchiveTask extends AbstractArchiveTask {
	@InputFile
	protected Object descriptor;
	@InputFiles
	protected List<Object> includedFiles = new ArrayList<>();
	
	public void descriptor(Object descriptor) {
		this.descriptor = descriptor;
		this.from(descriptor);
	}

	/**
	 * @return the descriptor
	 */
	public File getDescriptor() {
		FileCollectionFactory fcf = getFileCollectionFactory();
		return fcf.resolving(descriptor).getSingleFile();
	}

	/**
	 * @return the includedFiles
	 */
	public List<File> getIncludedFiles() {
		FileCollectionFactory fcf = getFileCollectionFactory();
		return includedFiles.stream()
			.map(f -> fcf.resolving(f).getSingleFile())
			.collect(Collectors.toList());
	}
	
	public void swf(Object swf) {
		this.includedFiles.add(swf);
		this.from(swf);
	}

    @Inject
    protected FileCollectionFactory getFileCollectionFactory() {
        throw new UnsupportedOperationException();
	}
	
	protected List<Object> buildParams() {
		// TODO: replace by copy stream reader (See Zip and ZipCopyAction classes in gradle sources)
		FileCollectionFactory fcf = getFileCollectionFactory();
		List<Object> params = new ArrayList<>();
		this.includedFiles.forEach(f -> {
			File file = fcf.resolving(f).getSingleFile();
			params.add("-C");
			params.add(file.getParentFile());
			params.add(file.getName());
		});
		return params;
	}
}