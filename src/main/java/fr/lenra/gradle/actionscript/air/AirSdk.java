package fr.lenra.gradle.actionscript.air;

import java.io.File;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;

import fr.lenra.gradle.actionscript.air.tool.Adt;
import fr.lenra.gradle.actionscript.sdk.Sdk;
import fr.lenra.util.VersionComparator;

public class AirSdk implements Sdk {
	private final Project project;
	private final String name;
	private final String version;
	private final String sdkVersion;
	private final String build;
	private final File location;
	private final Map<String, Integer> swfVersions;
	private final Map<String, String> applicationNamespaces;
	private final Map<String, String> extensionNamespaces;
	private final Adt adt;

	public AirSdk(Project project, String name, String version, String build, File location,
			Map<String, Integer> applicationNamespaces, Map<String, Integer> extensionNamespaces) {
		this.project = project;
		this.name = name;
		this.version = version;
		this.build = build;
		this.location = location;
		this.swfVersions = applicationNamespaces.entrySet().stream()
				.collect(Collectors.toMap(x -> getSdkVersion(x.getKey()), Map.Entry::getValue));
		this.sdkVersion = swfVersions.keySet().stream().max((a, b) -> new VersionComparator() {
		}.compare(a, b)).get();
		this.applicationNamespaces = applicationNamespaces.keySet().stream()
				.collect(Collectors.toMap(x -> getSdkVersion(x), Function.identity()));
		this.extensionNamespaces = extensionNamespaces.keySet().stream()
				.collect(Collectors.toMap(x -> getSdkVersion(x), Function.identity()));
		this.adt = project.getObjects().newInstance(Adt.class, project, this);
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the build
	 */
	public String getBuild() {
		return build;
	}

	/**
	 * @return the location
	 */
	public File getLocation() {
		return location;
	}

	public String getSdkVersion() {
		return this.sdkVersion;
	}

	/**
	 * @return the swfVersions
	 */
	public Map<String, Integer> getSwfVersions() {
		return swfVersions;
	}

	/**
	 * @return the applicationNamespaces
	 */
	public Map<String, String> getApplicationNamespaces() {
		return applicationNamespaces;
	}

	/**
	 * @return the extensionNamespaces
	 */
	public Map<String, String> getExtensionNamespaces() {
		return extensionNamespaces;
	}

	public FileCollection playerglobal() {
		return playerglobal(this.getSdkVersion());
	}

	public FileCollection playerglobal(String version) {
		return project.files(location.toPath().resolve("frameworks/libs/player/"+version+"/playerglobal.swc").toFile());
	}

	public FileCollection airglobal() {
		return project.files(location.toPath().resolve("frameworks/libs/air/airglobal.swc").toFile());
	}

	@Override
	public String toString() {
		return "SDK " + name + " : " + location;
	}

	private static String getSdkVersion(String namespace) {
		return namespace.substring(namespace.lastIndexOf('/')+1);
	}

	public Adt getAdt() {
		return this.adt;
	}
}