package fr.lenra.gradle.actionscript.air.tool.adt;

public enum AndroidTarget implements Target {
	APK, APK_CAPTIVE_RUNTIME, APK_DEBUG, APK_EMULATOR, APK_PROFILE
}