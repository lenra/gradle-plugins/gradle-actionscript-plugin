package fr.lenra.gradle.actionscript.air.target;

import javax.inject.Inject;

import org.gradle.api.Project;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.target.Target;

@Target("ios")
public class IosTarget extends AirTarget {
    private static final long serialVersionUID = 1L;
    
	@Inject
    public IosTarget(Project project, CompileConfiguration parentConfiguration) {
        super(project, parentConfiguration);
    }
}