package fr.lenra.gradle.actionscript.air.task;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.gradle.api.internal.file.FileCollectionFactory;
import org.gradle.api.internal.file.copy.CopyAction;
import org.gradle.api.internal.file.copy.CopyActionProcessingStream;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.WorkResult;

import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.actionscript.air.tool.adt.AirTarget;
import fr.lenra.gradle.actionscript.air.tool.adt.SigningOptions;
import fr.lenra.gradle.actionscript.air.tool.adt.SigningOptions.Storetype;
import fr.lenra.gradle.actionscript.air.tool.adt.Target;
import fr.lenra.gradle.plugin.ActionScriptExtension;

public abstract class AbstractAirPackageTask extends AbstractAirArchiveTask {

	@Input
	protected Target target = AirTarget.AIR;
	protected Object keystore;

	/**
	 * @return the target
	 */
	public Target getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(Target target) {
		this.target = target;
	}

	public void target(String t) {
		Class<? extends Target> c = getTargetClass();
		try {
			Method m = c.getMethod("valueOf", String.class);
			this.setTarget((Target) m.invoke(null, t.toUpperCase().replaceAll("-", "_")));
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Internal
	protected abstract Class<? extends Target> getTargetClass();

	public void keystore(Object keystore) {
		this.keystore = keystore;
		this.from(keystore);
	}

	@Override
	protected CopyAction createCopyAction() {
		return new CopyAction() {
			@Override
			public WorkResult execute(CopyActionProcessingStream copyAction) {
				getProject().getLogger().info("Build artifact for {}", getProject().getName());
				AirSdk sdk = (AirSdk) ((ActionScriptExtension) getProject().getExtensions().getByName("actionscript")).getSdks().getAt("air");
				FileCollectionFactory fcf = getFileCollectionFactory();
				File desc = fcf.resolving(descriptor).getSingleFile();
				SigningOptions opt = new SigningOptions();
				opt.keystore = fcf.resolving(keystore).getSingleFile();
				// TODO: detect the store type
				opt.storetype = Storetype.PKCS12;
				sdk.getAdt().packageApp(getArchiveFile().get().getAsFile(), target, opt, desc, buildParams());
				//throw new BuildException("Fatal build problems");
				return new WorkResult(){
				
					@Override
					public boolean getDidWork() {
						return true;
					}
				};
			}
		};
	}
}