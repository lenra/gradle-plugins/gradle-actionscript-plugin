package fr.lenra.gradle.actionscript.air.task;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;

import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.actionscript.air.tool.Adt;
import fr.lenra.gradle.actionscript.air.tool.adt.Platform;
import fr.lenra.gradle.plugin.ActionScriptExtension;

public class ListAirDevices extends DefaultTask {
	@Input
	private Platform platform;

	/**
	 * @return the platform
	 */
	public Platform getPlatform() {
		return platform;
	}

	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

	public void platform(String str) {
		Platform p = Platform.valueOf(str.toUpperCase());
		if (p==null)
			throw new RuntimeException("No platform found for "+str);
		this.platform = p;
	}

	@TaskAction
	public void listDevices() {
		if (this.platform==null)
			throw new NullPointerException("The platfomr must be defined");
			
		AirSdk sdk = (AirSdk) ((ActionScriptExtension) getProject().getExtensions().getByName("actionscript")).getSdks().getAt("air");
		Adt adt = sdk.getAdt();
		adt.listDevices(platform);
	}
}