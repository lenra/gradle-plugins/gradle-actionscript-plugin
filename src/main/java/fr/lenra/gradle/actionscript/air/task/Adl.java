package fr.lenra.gradle.actionscript.air.task;

import java.io.File;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.apache.tools.ant.taskdefs.condition.Os;
import org.gradle.api.DefaultTask;
import org.gradle.api.internal.file.FileCollectionFactory;
import org.gradle.api.internal.file.FileResolver;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.TaskAction;
import org.gradle.process.internal.ExecAction;
import org.gradle.process.internal.ExecActionFactory;

import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.plugin.ActionScriptExtension;

/**
 * Air Debug Launcher task
 */
public class Adl extends DefaultTask {
	@InputFile
	private Object descriptor;
	@InputDirectory @Optional
	private Object runtimeDirectory;
	@Input @Optional
	private String pubid;
	@Input
	private boolean nodebug;
	@Input @Optional
	private Profile profile;
	@InputDirectory @Optional
	private Object extensionsDirectory;
	@Input @Optional
	private String screenSize;

	/**
	 * @return the descriptor
	 */
	public File getDescriptor() {
		if (descriptor==null)
			return null;
		FileCollectionFactory fcf = this.getFileCollectionFactory();
		return fcf.resolving(descriptor).getSingleFile();
	}

	/**
	 * @return the runtimeDirectory
	 */
	public File getRuntimeDirectory() {
		if (runtimeDirectory==null)
			return null;
		FileCollectionFactory fcf = this.getFileCollectionFactory();
		return fcf.resolving(runtimeDirectory).getSingleFile();
	}

	/**
	 * @return the pubid
	 */
	public String getPubid() {
		return pubid;
	}

	/**
	 * @return the nodebug
	 */
	public boolean isNodebug() {
		return nodebug;
	}

	public String getScreenSize() {
		return screenSize;
	}

	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}

	/**
	 * @param pubid the pubid to set
	 */
	public void setPubid(String pubid) {
		this.pubid = pubid;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @return the extensionsDirectory
	 */
	public File getExtensionsDirectory() {
		if (extensionsDirectory==null)
			return null;
		FileCollectionFactory fcf = this.getFileCollectionFactory();
		return fcf.resolving(extensionsDirectory).getSingleFile();
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public void descriptor(Object descriptor) {
		this.descriptor = descriptor;
	}

	public void runtimeDirectory(Object runtimeDirectory) {
		this.runtimeDirectory = runtimeDirectory;
	}

	public void extensionsDirectory(Object extensionsDirectory) {
		this.extensionsDirectory = extensionsDirectory;
	}

	public void nodebug() {
		this.nodebug = true;
	}

	public void profile(String profile) {
		this.profile = Stream.of(Profile.values())
			.filter(p -> p.value.equals(profile))
			.findAny()
			.orElse(null);
	}

    @Inject
    protected FileCollectionFactory getFileCollectionFactory() {
        throw new UnsupportedOperationException();
    }

    @Inject
	protected FileResolver getFileResolver() {
		throw new UnsupportedOperationException();
	}

	@TaskAction
	public void run() {
		AirSdk sdk = (AirSdk) ((ActionScriptExtension) getProject().getExtensions().getByName("actionscript")).getSdks().getAt("air");
		StringBuilder path = new StringBuilder();
		path.append("bin/adl");
		if (Os.isFamily(Os.FAMILY_WINDOWS))
			path.append(".exe");
		ExecAction action = getServices().get(ExecActionFactory.class).newExecAction();
		action.setExecutable(sdk.getLocation().toPath().resolve(path.toString()));
		FileCollectionFactory fcf = this.getFileCollectionFactory();
		if (this.runtimeDirectory!=null)
			action.args("-runtime", fcf.resolving(this.runtimeDirectory).getSingleFile());
		if (this.pubid!=null)
			action.args("-pubid", this.pubid);
		if (this.nodebug)
			action.args("-nodebug");
		if (this.profile!=null)
			action.args("-profile", this.profile.value);
		if (this.extensionsDirectory!=null)
			action.args("-extdir", fcf.resolving(this.extensionsDirectory).getSingleFile());
		if (this.getScreenSize()!=null)
			action.args("-screensize", this.getScreenSize());
		action.args(fcf.resolving(descriptor).getSingleFile());
		getProject().getLogger().debug(String.join(" ", action.getCommandLine()));
		action.execute();
	}

	public static enum Profile {
		MOBILE_DEVICE("mobileDevice"), EXTENDED_MOBILE_DEVICE("extendedMobileDevice"),
		DESKTOP("desktop"), EXTENDED_DESKTOP("extendedDesktop"),
		TV("tv"), EXTENDED_TV("extendedTV");

		public final String value;

		private Profile(String val) {
			this.value = val;
		}
	}
}