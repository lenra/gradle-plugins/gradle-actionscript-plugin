package fr.lenra.gradle.actionscript.air.target;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.air.application.descriptor.AirAppDescriptor;
import fr.lenra.gradle.actionscript.air.application.descriptor.MobileAppDescriptor;
import fr.lenra.gradle.actionscript.air.task.Adl.Profile;
import fr.lenra.gradle.actionscript.target.Target;
import fr.lenra.gradle.sourceset.LanguageSourceSet;

@Target(value = "mobile", extend = "air", abstracted = true)
public class MobileTarget extends AirTarget {
	private static final long serialVersionUID = 1L;
	
	@Inject
	public MobileTarget(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
	}

	@Override
	protected AirAppDescriptor createAppDescriptor() {
		return new MobileAppDescriptor(this);
	}

	@Override
	public TaskProvider<? extends AbstractArchiveTask> createArchiveTask(LanguageSourceSet sourceSet, boolean debug,
			Configuration compileClasspath, Configuration runtimeClasspath, Configuration embedConfiguration) {
		TaskProvider<? extends AbstractArchiveTask> ret = super.createArchiveTask(sourceSet, debug, 
				compileClasspath, runtimeClasspath, embedConfiguration);
		
		if (isPackageAirArtifact() && !isLibrary() && debug)
			this.adl.configure(t -> t.setProfile(Profile.MOBILE_DEVICE));
		
		return ret;
	}
}