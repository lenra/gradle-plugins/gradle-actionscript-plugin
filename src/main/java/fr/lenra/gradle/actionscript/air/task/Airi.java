package fr.lenra.gradle.actionscript.air.task;

import java.io.File;

import org.gradle.api.internal.file.FileCollectionFactory;
import org.gradle.api.internal.file.copy.CopyAction;
import org.gradle.api.internal.file.copy.CopyActionProcessingStream;
import org.gradle.api.tasks.WorkResult;

import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.plugin.ActionScriptExtension;

public class Airi extends AbstractAirArchiveTask {

	public Airi() {
		this.getArchiveExtension().set("airi");
	}

	@Override
	protected CopyAction createCopyAction() {
		return new CopyAction() {
			@Override
			public WorkResult execute(CopyActionProcessingStream copyAction) {
				getProject().getLogger().info("Build artifact for {}", getProject().getName());
				AirSdk sdk = (AirSdk) ((ActionScriptExtension) getProject().getExtensions().getByName("actionscript")).getSdks().getAt("air");
				FileCollectionFactory fcf = getFileCollectionFactory();
				File desc = fcf.resolving(descriptor).getSingleFile();
				// TODO: replace by copy stream reader (See Zip and ZipCopyAction classes in gradle sources)
				sdk.getAdt().prepare(getArchiveFile().get().getAsFile(), desc, buildParams());
				//throw new BuildException("Fatal build problems");
				return new WorkResult(){
				
					@Override
					public boolean getDidWork() {
						return true;
					}
				};
			}
		};
	}
}