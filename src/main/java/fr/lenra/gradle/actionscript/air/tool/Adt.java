package fr.lenra.gradle.actionscript.air.tool;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.apache.tools.ant.taskdefs.condition.Os;
import org.gradle.api.Project;
import org.gradle.process.ExecResult;
import org.gradle.process.internal.ExecAction;
import org.gradle.process.internal.ExecActionFactory;

import fr.lenra.gradle.actionscript.air.AirSdk;
import fr.lenra.gradle.actionscript.air.tool.adt.AndroidTarget;
import fr.lenra.gradle.actionscript.air.tool.adt.Platform;
import fr.lenra.gradle.actionscript.air.tool.adt.SigningOptions;
import fr.lenra.gradle.actionscript.air.tool.adt.Target;

public class Adt {
	private Project project;
	private ExecActionFactory execFactory;
	private AirSdk sdk;
	private String path;

	@Inject
	public Adt(ExecActionFactory execFactory, Project project, AirSdk sdk) {
		this.project = project;
		this.execFactory = execFactory;
		this.sdk = sdk;
		StringBuilder sb = new StringBuilder();
		sb.append("bin/adt");
		if (Os.isFamily(Os.FAMILY_WINDOWS))
			sb.append(".bat");
		this.path = sb.toString();
	}

	private int execute(Object... args) {
		return this.execute(Arrays.asList(args));
	}

	private int execute(Iterable<?> args) {
		ExecAction action = this.execFactory.newExecAction();
		action.setExecutable(sdk.getLocation().toPath().resolve(path.toString()));
		action.args(args);
		project.getLogger().debug(String.join(" ", action.getCommandLine()));
		ExecResult result = action.execute();
		return result.getExitValue();
	}

	public void listDevices(Platform platform) {
		execute("-devices", 
			"-platform", platform.name().toLowerCase());
	}

	public void prepare(File output, File descriptor, Collection<Object> params) {
		List<Object> args = new ArrayList<>();
		args.add("-prepare");
		args.add(output);
		args.add(descriptor);
		args.addAll(params);
		execute(args);
	}

	public void packageApp(File output, Target target, SigningOptions signing, File descriptor, Collection<Object> params) {
		List<Object> args = new ArrayList<>();
		args.add("-package");
		// TODO: add iOS
		boolean mobileTarget = target!=null && target instanceof AndroidTarget;
		if (mobileTarget) {
			args.add("-target");
			args.add(target.getValue());
		}
		if (signing!=null) {
			args.add("-storetype");
			args.add(signing.storetype.name().toLowerCase());
			args.add("-keystore");
			args.add(signing.keystore);
			if (signing.storepass==null) {
				this.sdk.getProject().getAnt().invokeMethod("input", new HashMap<String, Object>(){
					private static final long serialVersionUID = 1L;
				{
					put("message", "Enter the password for the keystore "+ signing.keystore);
					put("addproperty", "signingStorePass");
				}});
				signing.storepass = (String) this.sdk.getProject().getAnt().getProperty("signingStorePass");
				// signing.storepass = new String(System.console().readPassword("Enter the password for the keystore {}", signing.keystore));
			}
			args.add("-storepass");
			args.add(signing.storepass);
		}
		if (target!=null && !mobileTarget) {
			args.add("-target");
			args.add(target.getValue());
		}
		args.add(output);
		args.add(descriptor);
		args.addAll(params);
		execute(args);
	}
}