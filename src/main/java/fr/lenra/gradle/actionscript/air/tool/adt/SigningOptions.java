package fr.lenra.gradle.actionscript.air.tool.adt;

import java.io.File;

public class SigningOptions {
	public Storetype storetype;
	public File keystore;
	public String storepass;

	public static enum Storetype {
		PKCS12
	}
}