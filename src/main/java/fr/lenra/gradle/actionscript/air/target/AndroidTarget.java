package fr.lenra.gradle.actionscript.air.target;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.air.application.descriptor.AirAppDescriptor;
import fr.lenra.gradle.actionscript.air.application.descriptor.AndroidAppDescriptor;
import fr.lenra.gradle.actionscript.air.task.AbstractAirPackageTask;
import fr.lenra.gradle.actionscript.air.task.Apk;
import fr.lenra.gradle.actionscript.target.Target;
import fr.lenra.gradle.sourceset.LanguageSourceSet;

/**
 * See https://developer.android.com/reference/android/Manifest.permission to
 * get the list of permissions
 */
@Target(value = "android", extend = "mobile")
public class AndroidTarget extends MobileTarget {
	private static final long serialVersionUID = 1L;
	
	private transient ListProperty<String> permissions;

	@Inject
	public AndroidTarget(Project project, CompileConfiguration parentConfiguration) {
		super(project, parentConfiguration);
		permissions = project.getObjects().listProperty(String.class);
		permissions.convention(new ArrayList<>());
	}

	@Override
	protected AirAppDescriptor createAppDescriptor() {
		return new AndroidAppDescriptor(this);
	}

	public Collection<String> getPermissions() {
		return permissions.get();
	}

	public void setPermissions(Collection<String> permissions) {
		this.permissions.set(permissions);
	}

	@Override
	protected Class<? extends AbstractAirPackageTask> getPackageTaskClass() {
		return Apk.class;
	}

	@Override
	public TaskProvider<? extends AbstractArchiveTask> createArchiveTask(LanguageSourceSet sourceSet, boolean debug,
			Configuration compileClasspath, Configuration runtimeClasspath, Configuration embedConfiguration) {
		TaskProvider<? extends AbstractArchiveTask> ret = super.createArchiveTask(sourceSet, debug,
				compileClasspath, runtimeClasspath, embedConfiguration);
		if (isPackageAirArtifact() && !isLibrary() && debug) {
			ret.configure(t -> {
				((Apk)t).setTarget(fr.lenra.gradle.actionscript.air.tool.adt.AndroidTarget.APK_DEBUG);
			});
		}
		return ret;
	}
}