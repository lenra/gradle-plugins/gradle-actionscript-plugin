package fr.lenra.gradle.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.provider.ListProperty;

import fr.lenra.gradle.actionscript.CompileConfiguration;
import fr.lenra.gradle.actionscript.sdk.SdkContainer;
import fr.lenra.gradle.actionscript.sdk.internal.DefaultSdkContainer;
import fr.lenra.gradle.actionscript.target.TargetContainer;
import fr.lenra.gradle.actionscript.target.internal.DefaultTargetContainer;
import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.plugin.language.LanguageExtension;
import groovy.lang.Closure;

public class ActionScriptExtension extends LanguageExtension {

	private final ListProperty<String> defaultTargets;
	private List<String> targetPackages;
	private final DefaultTargetContainer targets;
	private final CompileConfiguration configuration;
	private final SdkContainer sdks;

	private boolean library;

	@Inject
	public ActionScriptExtension(Project project, Language language) {
		super(project, language);
		this.configuration = new CompileConfiguration(project);
		this.targets = project.getObjects().newInstance(DefaultTargetContainer.class, project, this, configuration);
		defaultTargets = project.getObjects().listProperty(String.class);
		defaultTargets.convention(Arrays.asList("swf"));
		this.targetPackages = new ArrayList<>();
		this.targetPackages.add("fr/lenra/gradle/actionscript/target");
		this.sdks = project.getObjects().newInstance(DefaultSdkContainer.class, project);
	}

	public TargetContainer getTargets() {
		return this.targets;
	}
	
	public TargetContainer targets(Closure closure) {
		this.targets.configure(closure);
		return this.targets;
	}

	public CompileConfiguration getConfiguration() {
		return configuration;
	}

	public CompileConfiguration configuration(Closure closure) {
		closure.setDelegate(configuration);
		closure.run();
		return configuration;
	}

	/**
	 * @return the library
	 */
	public boolean isLibrary() {
		return library;
	}

	/**
	 * @param library the library to set
	 */
	public void setLibrary(boolean library) {
		this.library = library;
	}

	/**
	 * @return the defaultTargets
	 */
	public List<String> getDefaultTargets() {
		return defaultTargets.get();
	}

	/**
	 * @param defaultTargets the defaultTargets to set
	 */
	public void setDefaultTargets(List<String> defaultTargets) {
		this.defaultTargets.set(defaultTargets);
	}

	/**
	 * @return the targetPackages
	 */
	public List<String> getTargetPackages() {
		return targetPackages;
	}

	/**
	 * @param targetPackages the targetPackages to set
	 */
	public void setTargetPackages(List<String> targetPackages) {
		this.targetPackages = targetPackages;
	}

	/**
	 * @return the sdks
	 */
	public SdkContainer getSdks() {
		return sdks;
	}
}