package fr.lenra.gradle.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.internal.project.ProjectInternal;

public class ActionScriptLibraryPlugin implements Plugin<ProjectInternal> {

	@Override
	public void apply(ProjectInternal project) {
		project.getLogger().info("Apply library plugin");
		project.getPluginManager().apply("fr.lenra.gradle.actionscript");
		project.getExtensions().configure(ActionScriptExtension.class, e -> e.setLibrary(true));
	}
}