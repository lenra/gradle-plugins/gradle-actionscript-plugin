package fr.lenra.gradle.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.inject.Inject;

import org.gradle.api.NamedDomainObjectProvider;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.artifacts.ConfigurationPublications;
import org.gradle.api.artifacts.PublishArtifact;
import org.gradle.api.attributes.Attribute;
import org.gradle.api.attributes.AttributesSchema;
import org.gradle.api.component.AdhocComponentWithVariants;
import org.gradle.api.internal.artifacts.dsl.LazyPublishArtifact;
import org.gradle.api.internal.plugins.DefaultArtifactPublicationSet;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.plugins.HelpTasksPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;
import org.gradle.util.GUtil;

import fr.lenra.gradle.actionscript.target.AbstractTargetCompiler;
import fr.lenra.gradle.actionscript.target.Target;
import fr.lenra.gradle.actionscript.target.TargetContainer;
import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.plugin.language.LanguageConvention;
import fr.lenra.gradle.plugin.language.LanguageExtension;
import fr.lenra.gradle.plugin.language.LanguagePlugin;
import fr.lenra.gradle.plugin.language.LanguagePluginConvention;
import fr.lenra.gradle.sourceset.LanguageSourceSet;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;
import fr.lenra.gradle.sourceset.internal.DefaultLanguageSourceSet;
import fr.lenra.gradle.task.ActionScriptCompile;
import fr.lenra.gradle.task.TargetsTask;

public class ActionScriptPluginConvention extends LanguageConvention {
	public static final Attribute<Boolean> DEBUG_ATTRIBUTE = Attribute.of("actionscript.debug", Boolean.class);
	public static final String DEBUG_SOURCE_SET_NAME = "debug";
	private static final List<String> CONFIGURATIONS = Arrays.asList("default", "implementation", "compileOnly", "runtimeOnly", "compileClasspath", "runtimeClasspath", "embed", "embedClasspath");
	protected Map<String, org.gradle.api.artifacts.Configuration> initialConfigs;
	private final ObjectFactory objectFactory;
	private ActionScriptExtension extension;
	private LanguagePluginConvention languageConvention;

	@Inject
	public ActionScriptPluginConvention(final Project project, final Language language,
			final LanguageExtension extension, final ObjectFactory objectFactory) {
		super(project, language, extension);
		this.objectFactory = objectFactory;
	}

	@Override
	public void configure() {
		this.getProject().getLogger().info("Configure ActionScriptConvention");

		AttributesSchema attributesSchema = this.getProject().getDependencies().getAttributesSchema();
		if (!attributesSchema.hasAttribute(Target.TARGET_ATTRIBUTE))
			attributesSchema.attribute(Target.TARGET_ATTRIBUTE/*, a -> {
				// a.getCompatibilityRules().
				System.out.println(a);
			}*/);
		if (!attributesSchema.hasAttribute(DEBUG_ATTRIBUTE))
			attributesSchema.attribute(DEBUG_ATTRIBUTE);

		final ConfigurationContainer configurations = this.getProject().getConfigurations();
		final TaskContainer tasks = this.getProject().getTasks();
		this.languageConvention = (LanguagePluginConvention) this.getProject().getConvention()
				.getPlugins().get(LanguagePlugin.PLUGIN_CONVENTION_NAME);
		this.extension = (ActionScriptExtension) this.getExtension();
		final LanguageSourceSetContainer sourceSets = languageConvention.getSourceSets();

		createBaseTasks(extension, tasks);

		this.initialConfigs = new HashMap<>(configurations.getAsMap());

		final TargetContainer targets = extension.getTargets();

		sourceSets.all(s -> {
			NamedDomainObjectProvider<Configuration> embed = configurations.register(((DefaultLanguageSourceSet)s).getConfigName("embed"), config -> {
				config.setVisible(false);
				config.setDescription("Embed dependencies for " + s.getName() + ".");
				config.setCanBeConsumed(false);
				config.setCanBeResolved(false);
				config.setTransitive(false);
				if (!SourceSet.MAIN_SOURCE_SET_NAME.equals(s.getName())) {
					this.getProject().getLogger().debug("{} extendsFrom {}", config, "embed");
					config.extendsFrom(configurations.getByName("embed"));
				}
			});
			configurations.register(((DefaultLanguageSourceSet)s).getConfigName("embedClasspath"), config -> {
				config.setVisible(false);
				config.setDescription("Embed classpath for " + s.getName() + ".");
				config.setCanBeConsumed(false);
				config.setCanBeResolved(true);
				config.setTransitive(false);
				config.extendsFrom(embed.get());
			});
			targets.all(t -> {
				createTargetConfigs(t.getName(), s, configurations);
				configureTargetConfigs(t, s, configurations);
			});
		});

		createDebugSourceSet(sourceSets, configurations);

		this.getProject().afterEvaluate(p -> {
			createTasks();
		});
	}

	protected void createDebugSourceSet(final LanguageSourceSetContainer sourceSets,
			final ConfigurationContainer configurations) {
		this.getProject().getLogger().info("Create ActionScript debug sourceSet");
		DefaultLanguageSourceSet debug = (DefaultLanguageSourceSet) sourceSets.maybeCreate(DEBUG_SOURCE_SET_NAME);
		LanguageSourceSet main = sourceSets.getAt(SourceSet.MAIN_SOURCE_SET_NAME);
		Configuration defaultConfig = configurations.getAt("default");
		main.all(d -> debug.getAt(d.getName()).source(d));
		Configuration debugConfig = configurations.create(DEBUG_SOURCE_SET_NAME, c -> {
			c.setDescription("Debug elements configuration");
			c.setVisible(defaultConfig.isVisible());
			c.setCanBeConsumed(defaultConfig.isCanBeConsumed());
			c.setCanBeResolved(defaultConfig.isCanBeResolved());
		});
		configurations.getAt(debug.getConfigName("implementation")).extendsFrom(configurations.getAt("implementation"));
		configurations.getAt(debug.getConfigName("compileOnly")).extendsFrom(configurations.getAt("compileOnly"));
		configurations.getAt(debug.getConfigName("runtimeOnly")).extendsFrom(configurations.getAt("runtimeOnly"));
		debugConfig.extendsFrom(configurations.getAt(debug.getConfigName("runtimeClasspath")));
	}

	protected void createBaseTasks(final ActionScriptExtension extension, final TaskContainer tasks) {
		this.getProject().getLogger().info("Create ActionScript base tasks");

		this.getProject().getLogger().debug("Create task actionScriptTargets");
		final TargetsTask targetsTask = tasks.maybeCreate("actionScriptTargets", TargetsTask.class);
		targetsTask.setGroup(HelpTasksPlugin.HELP_GROUP);
		targetsTask.setImpliesSubProjects(true);
		targetsTask.setDescription("Displays the ActionScript build targets declared in root project '"
				+ this.getProject().getName() + "'.");
	}

	protected void createTargetConfigs(final String name, final LanguageSourceSet sourceSet,
			final ConfigurationContainer configurations) {
		this.getProject().getLogger().info("Create {} target {} configurations", sourceSet.getName(), name);

		// For each existing configuration, define a configuration prefixed by the
		// target name
		CONFIGURATIONS.stream()
			.filter(n -> !("default".equals(n) && SourceSet.TEST_SOURCE_SET_NAME.equals(sourceSet.getName())))
			.forEach(baseConfig -> {
				Configuration config = configurations.getAt(((DefaultLanguageSourceSet) sourceSet).getConfigName(baseConfig));
				final String configName = getConfigName(baseConfig, name, sourceSet);

				this.getProject().getLogger().info("Create configuration {}", configName);
				NamedDomainObjectProvider<Configuration> provider = configurations.register(configName, newConfig -> {
					newConfig.setVisible(config.isVisible());
					newConfig.setCanBeConsumed(config.isCanBeConsumed());
					newConfig.setCanBeResolved(config.isCanBeResolved());
					newConfig.setDescription(config.getDescription() + " [target : " + name + "]");
					if (newConfig.isCanBeResolved() || newConfig.isCanBeConsumed()) {
						// apiElementsConfiguration.getAttributes().attribute(USAGE_ATTRIBUTE, objectFactory.named(Usage.class, Usage.JAVA_API));
						// newConfig.getAttributes().attribute(LIBRARY_ELEMENTS_ATTRIBUTE, objectFactory.named(LibraryElements.class, LibraryElements.JAR));
						// apiElementsConfiguration.getAttributes().attribute(BUNDLING_ATTRIBUTE, objectFactory.named(Bundling.class, EXTERNAL));
						// newConfig.getAttributes().attribute(Category.CATEGORY_ATTRIBUTE, objectFactory.named(Category.class, Category.LIBRARY));
						newConfig.getAttributes().attribute(Target.TARGET_ATTRIBUTE, name);
						newConfig.getAttributes().attribute(DEBUG_ATTRIBUTE, DEBUG_SOURCE_SET_NAME.equals(sourceSet.getName()));
					}
				});
				Configuration conf = provider.get();
				if ("default".equals(baseConfig)) {
					AdhocComponentWithVariants component = (AdhocComponentWithVariants) this.getProject().getComponents().getByName("actionscript");
					this.getProject().getLogger().info("Create actionscript variant {}", configName);
					component.addVariantsFromConfiguration(conf, details -> {
						// System.out.println("details : "+details);
						// ConfigurationVariant variant = details.getConfigurationVariant();
						// System.out.println("variant : "+variant);
						// boolean satisfied = false;
						// for (PublishArtifact art : variant.getArtifacts()) {
						// 	System.out.println(art + " : " + art.getType());
						// 	if (JavaBasePlugin.UNPUBLISHABLE_VARIANT_ARTIFACTS.contains(art.getType())) {
						// 		satisfied = true;
						// 		break;
						// 	}
						// }
						// if (satisfied)
						// 	details.skip();
						// else
						// 	details.mapToMavenScope("runtime");
						// details.mapToOptional();
					});
				}
			});
	}

	protected void configureTargetConfigs(final AbstractTargetCompiler target, final LanguageSourceSet sourceSet,
			final ConfigurationContainer configurations) {
		final String name = target.getName();
		this.getProject().getLogger().info("Configure {} target {} configurations", sourceSet.getName(), name);
		LanguageSourceSet main = this.languageConvention.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);

		// For each existing configuration, define a configuration prefixed by the
		// target name
		CONFIGURATIONS.stream()
			.filter(n -> !("default".equals(n) && SourceSet.TEST_SOURCE_SET_NAME.equals(sourceSet.getName())))
			.forEach(baseConfig -> {
				Configuration config = configurations.getAt(((DefaultLanguageSourceSet) sourceSet).getConfigName(baseConfig));
				String configName = getConfigName(baseConfig, name, sourceSet);
				Configuration newConfig = configurations.getAt(configName);

				copyConfigurationProperties(config, newConfig, conf -> {
					if (conf.startsWith(sourceSet.getName()))
						return getConfigName(conf.substring(sourceSet.getName().length()), name, sourceSet);
					return GUtil.toLowerCamelCase(name + " " + conf);
				});

				if (!(newConfig.isCanBeConsumed() || newConfig.isCanBeResolved())) {
					if (target.getParentConfiguration() instanceof AbstractTargetCompiler) {
						AbstractTargetCompiler parent = (AbstractTargetCompiler) target.getParentConfiguration();
						this.getProject().getLogger().debug("{} extendsFrom {}", newConfig, getConfigName(baseConfig, parent.getName(), sourceSet));
						newConfig.extendsFrom(configurations.getAt(getConfigName(baseConfig, parent.getName(), sourceSet)));
					}
					else {
						this.getProject().getLogger().debug("{} extendsFrom {}", newConfig, config);
						newConfig.extendsFrom(config);
					}
					if (!SourceSet.MAIN_SOURCE_SET_NAME.equals(sourceSet.getName())) {
						this.getProject().getLogger().debug("{} extendsFrom {}", newConfig, getConfigName(baseConfig, name, main));
						newConfig.extendsFrom(configurations.getAt(getConfigName(baseConfig, name, main)));
					}
				}
			});
	}

	private void copyConfigurationProperties(Configuration source, Configuration destination, Function<String, String> nameMapper) {
		Set<Configuration> ext = source.getExtendsFrom();
		String configName = destination.getName();

		List<String> extList = new ArrayList<>();
		if (!ext.isEmpty()) {
			ext.stream().forEach(conf -> extList.add(nameMapper.apply(conf.getName())));
		}
		if (!extList.isEmpty()) {
			getProject().getConfigurations().configureEach(c -> {
				if (extList.contains(c.getName())) {
					this.getProject().getLogger().debug("{} extendsFrom {}", configName, c.getName());
					destination.extendsFrom(c);
				}
			});
		}
	}

	private String getConfigName(final String configName, final String targetName, final LanguageSourceSet sourceSet) {
		String name = configName;
		if ("default".equals(name))
			name = "";
		return ((DefaultLanguageSourceSet) sourceSet).getConfigName(targetName + " " + name);
	}

	protected void createTargetBuildTask(final AbstractTargetCompiler target, final LanguageSourceSet sourceSet,
			final TaskContainer tasks, final ConfigurationContainer configurations) {
		String sourceSetName = sourceSet.getName();
		if (!SourceSet.TEST_SOURCE_SET_NAME.equals(sourceSetName)) {
			if (target.getIsAbstract())
				return;
			String name = target.getName();
			String depName = sourceSetName.equals(SourceSet.MAIN_SOURCE_SET_NAME) ? "" : sourceSetName;
			String languageTaskName = GUtil.toLowerCamelCase("compile " + depName + " actionscript");
			ActionScriptCompile languageTask = (ActionScriptCompile) tasks.getAt(languageTaskName);

			TaskProvider<? extends AbstractArchiveTask> task = target.createArchiveTask(sourceSet,
				DEBUG_SOURCE_SET_NAME.equals(sourceSetName),
				configurations.getAt(getConfigName("compileClasspath", name, sourceSet)),
				configurations.getAt(getConfigName("runtimeClasspath", name, sourceSet)),
				configurations.getAt(getConfigName("embedClasspath", name, sourceSet)));
			this.getProject().getLogger().info("Create target {} build task for sourceSet {}", name,
					sourceSet.getName());
			
			languageTask.dependsOn(task);
			this.getProject().getLogger().debug("Archive task {} created", task.getName());
			PublishArtifact artifact = new LazyPublishArtifact(task);

			Configuration conf = this.getProject().getConfigurations().getAt(getConfigName("default", name, sourceSet));
			this.getProject().getExtensions().getByType(DefaultArtifactPublicationSet.class).addCandidate(artifact);
			this.getProject().artifacts(artifacts -> {
				artifacts.add(conf.getName(), artifact);
			});
			ConfigurationPublications publications = conf.getOutgoing();
			publications.getArtifacts().add(artifact);
		}
	}

	public void createTasks() {
		LanguagePluginConvention languageConvention = (LanguagePluginConvention) this.getProject().getConvention()
				.getPlugins().get(LanguagePlugin.PLUGIN_CONVENTION_NAME);
		ActionScriptExtension extension = (ActionScriptExtension) this.getExtension();
		LanguageSourceSetContainer sourceSets = languageConvention.getSourceSets();
		TargetContainer targets = extension.getTargets();
		TaskContainer tasks = this.getProject().getTasks();
		ConfigurationContainer configurations = this.getProject().getConfigurations();

		// create default targets
		if (targets.isEmpty()) {
			final List<String> defaultTargets = extension.getDefaultTargets();
			this.getProject().getLogger().debug("Add default targets : {}",
			defaultTargets);
			defaultTargets.stream().forEach(t -> {
				if (targets.findByName(t) == null)
					targets.create(t, target -> target.setIsAbstract(false));
			});
		}

		// create tasks
		sourceSets.all(s -> {
			extension.getTargets().all(t -> {
				createTargetBuildTask(t, s, tasks, configurations);
			});
		});
	}
}