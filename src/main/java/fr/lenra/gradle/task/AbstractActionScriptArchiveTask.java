package fr.lenra.gradle.task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.royale.compiler.Messages;
import org.apache.royale.compiler.clients.problems.CompilerProblemCategorizer;
import org.apache.royale.compiler.clients.problems.ProblemFormatter;
import org.apache.royale.compiler.clients.problems.ProblemPrinter;
import org.apache.royale.compiler.clients.problems.ProblemQuery;
import org.apache.royale.compiler.clients.problems.WorkspaceProblemFormatter;
import org.apache.royale.compiler.config.Configuration;
import org.apache.royale.compiler.exceptions.ConfigurationException;
import org.apache.royale.compiler.internal.projects.RoyaleProject;
import org.apache.royale.compiler.internal.projects.RoyaleProjectConfigurator;
import org.apache.royale.compiler.internal.targets.AppSWFTarget;
import org.apache.royale.compiler.internal.targets.SWFTarget;
import org.apache.royale.compiler.internal.workspaces.Workspace;
import org.apache.royale.compiler.problems.ICompilerProblem;
import org.apache.royale.compiler.projects.ProjectFactory;
import org.apache.royale.compiler.targets.ITarget.TargetType;
import org.apache.royale.compiler.targets.ITargetSettings;
import org.apache.royale.swf.Header;
import org.apache.royale.swf.ISWF;
import org.apache.royale.swf.io.ISWFWriter;
import org.apache.royale.swf.io.ISWFWriterFactory;
import org.apache.royale.swf.io.SizeReportWritingSWFWriter;
import org.apache.tools.ant.BuildException;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.file.copy.CopyAction;
import org.gradle.api.internal.file.copy.CopyActionProcessingStream;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.WorkResult;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import fr.lenra.gradle.actionscript.CompileConfiguration;

public class AbstractActionScriptArchiveTask extends AbstractArchiveTask {
	// private CompileConfiguration compileConfiguration;
	@InputFiles
	private SourceDirectorySet sourceDirs;
	@InputFiles
	private FileCollection compileClasspath = getProject().files();
	@InputFiles
	private FileCollection runtimeClasspath = getProject().files();
	@InputFiles
	private Collection<File> compileDependencies;
	@InputFiles
	private Collection<File> runtimeDependencies;
	protected RoyaleProject project;
	protected RoyaleProjectConfigurator configurator;
	protected ProblemQuery problems;

	@Input
	private boolean debug;
	@Input @Optional
	private String debugPassword;
	@Input @Optional
	private String targetPlayer;
	@Input @Optional
	private Integer swfVersion;
	@Input @Optional
	private Provider<Map<String, String>> defines;

	/**
	 * @return the debug
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * @param debug the debug to set
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	/**
	 * @return the debugPassword
	 */
	public String getDebugPassword() {
		return debugPassword;
	}

	/**
	 * @param debugPassword the debugPassword to set
	 */
	public void setDebugPassword(String debugPassword) {
		this.debugPassword = debugPassword;
	}

	/**
	 * @return the defines
	 */
	public Map<String, String> getDefines() {
		if (defines == null || !defines.isPresent())
			return new HashMap<>();
		return defines.get();
	}

	/**
	 * @param defines the defines to set
	 */
	public void setDefines(Map<String, String> defines) {
		this.defines = getProject().provider(() -> defines);
	}

	public void setDefines(Provider<Map<String, String>> defines) {
		this.defines = defines;
	}

	/**
	 * @return the targetPlayer
	 */
	public String getTargetPlayer() {
		return targetPlayer;
	}

	/**
	 * @param targetPlayer the targetPlayer to set
	 */
	public void setTargetPlayer(String targetPlayer) {
		this.targetPlayer = targetPlayer;
	}

	/**
	 * @param swfVersion the swfVersion to set
	 */
	public void setSwfVersion(Integer swfVersion) {
		this.swfVersion = swfVersion;
	}

	/**
	 * @return the swfVersion
	 */
	public Integer getSwfVersion() {
		return swfVersion;
	}

	/**
	 * @deprecated since version 1.5.0 and will be removed in version 2.0.0
	 * @return the compileConfiguration
	 */
	@Deprecated @Internal
	public CompileConfiguration getCompileConfiguration() {
		return null;
	}

	/**
	 * Defines the compile configuration to use the defines variables of it
	 * 
	 * @deprecated since version 1.5.0 and will be removed in version 2.0.0
	 * @param compileConfiguration the compileConfiguration to set
	 */
	@Deprecated
	public void setCompileConfiguration(CompileConfiguration compileConfiguration) {
		if (compileConfiguration != null)
			this.setDefines(compileConfiguration.getDefines());
	}

	/**
	 * @return the sourceDirs
	 */
	public SourceDirectorySet getSourceDirs() {
		return sourceDirs;
	}

	/**
	 * @param sourceDirs the sourceDirs to set
	 */
	public void setSourceDirs(SourceDirectorySet sourceDirs) {
		this.sourceDirs = sourceDirs;
		this.from(sourceDirs);
	}

	public FileCollection getCompileClasspath() {
		return this.compileClasspath;
	}

	/**
	 * @param compileClasspath the compileClasspath to set
	 */
	public void setCompileClasspath(FileCollection compileClasspath) {
		this.compileClasspath = compileClasspath;
		this.compileDependencies = compileClasspath.getFiles();
	}

	public FileCollection getRuntimeClasspath() {
		return this.runtimeClasspath;
	}

	/**
	 * @param runtimeClasspath the runtimeClasspath to set
	 */
	public void setRuntimeClasspath(FileCollection runtimeClasspath) {
		this.runtimeClasspath = runtimeClasspath;
		this.runtimeDependencies = runtimeClasspath.getFiles();
	}

	/**
	 * @return the compileDependencies
	 */
	public Collection<File> getCompileDependencies() {
		return compileDependencies;
	}

	/**
	 * @param compileDependencies the compileDependencies to set
	 */
	public void setCompileDependencies(Collection<File> compileDependencies) {
		this.compileDependencies = compileDependencies;
	}

	/**
	 * @return the runtimeDependencies
	 */
	public Collection<File> getRuntimeDependencies() {
		return runtimeDependencies;
	}

	/**
	 * @param runtimeDependencies the runtimeDependencies to set
	 */
	public void setRuntimeDependencies(Collection<File> runtimeDependencies) {
		this.runtimeDependencies = runtimeDependencies;
	}

	protected void configure() {
		this.getProject().getLogger().info("Start SWF build");
		project = (RoyaleProject) ProjectFactory.createSimpleRoyaleProject();
		configurator = new RoyaleProjectConfigurator(Configuration.class);

		this.getProject().getLogger().debug("Configure target SWF build");
		configurator.setOutput(getArchiveFile().get().getAsFile());
		configurator.enableAccessibility(true);
		configurator.optimize(true);
		configurator.setExcludeNativeJSLibraries(true);
		configurator.allowSourcePathOverlap(false);
		configurator.useNetwork(true);
		if (targetPlayer!=null) {
			if (!targetPlayer.matches("\\d+(.\\d+){0,2}"))
				throw new RuntimeException("The target player version doesn't seems to be write");
			String[] version = targetPlayer.split("[.]");
			int major = Integer.parseInt(version[0]);
			int minor = 0;
			if (version.length>1)
				minor = Integer.parseInt(version[1]);
			int revision = 0;
			if (version.length>2)
				revision = Integer.parseInt(version[2]);
			configurator.setTargetPlayer(major, minor, revision);
		}

		if (debug)
			configurator.enableDebugging(true, debugPassword);

		Map<String, String> defines = getDefines();
		defines.put("CONFIG::debug", Boolean.toString(debug));
		configurator.setDefineDirectives(defines);

		if (sourceDirs == null)
			throw new RuntimeException("No source directories are defined");

		Collection<File> srcDirs = sourceDirs.getSrcDirs().stream()
			.filter(f -> f.exists())
			.collect(Collectors.toList());
		configurator.addSourcePath(srcDirs);

		configurator.applyToProject(project);

		problems = new ProblemQuery(configurator.getCompilerProblemSettings());
	}

	protected void addDependencies() {
		this.getProject().getLogger().info("Manage dependencies");
		if (this.compileDependencies != null && this.runtimeDependencies != null) {
			this.compileDependencies = compileDependencies.stream().filter(f -> !runtimeDependencies.contains(f))
					.collect(Collectors.toList());
		}
		if (this.compileDependencies != null)
			configurator.addExternalLibraryPath(this.compileDependencies);
		if (this.runtimeDependencies != null)
			configurator.addLibraryPath(this.runtimeDependencies);

		configurator.applyToProject(project);
	}

	protected void build() {
		this.getProject().getLogger().info("Build archive");
		this.getProject().getLogger().debug("Create SWF settings");
		ITargetSettings targetSettings = configurator.getTargetSettings(TargetType.SWF);

		this.getProject().getLogger().debug("Get configuration");
		Configuration config = configurator.getConfiguration();
		if (swfVersion!=null) {
			try {
				config.setSwfVersion(null, swfVersion);
			} catch (ConfigurationException e) {
				throw new RuntimeException("Failed to define the SWF version", e);
			}
		}
		// TODO: manage ByteArray embed class (doesn't work)
		// config.setCompilerByteArrayEmbedClass(null, "org.apache.flex.core.ByteArrayAsset");
		Messages.setLocale(config.getToolsLocale());
		project.apiReportFile = config.getApiReport();
		problems.addAll(configurator.getConfigurationProblems());

		problems.addAll(project.getFatalProblems());

		if (problems.hasErrors())
			return;

		this.getProject().getLogger().debug("Create SWF target");

		SWFTarget target;

		final Header.Compression compression = Header.decideCompression(targetSettings.useCompression(),
				targetSettings.getSWFVersion(), targetSettings.isDebugEnabled());
		final ISWFWriterFactory writerFactory = SizeReportWritingSWFWriter
				.getSWFWriterFactory(targetSettings.getSizeReport());
		final File outputFile = new File(config.getOutput());
		
		target = new AppSWFTarget(project, targetSettings, null);

		final List<ICompilerProblem> problemsBuildingSWF = new ArrayList<ICompilerProblem>();
		final ISWF swf = target.build(problemsBuildingSWF);
		problems.addAll(problemsBuildingSWF);

		if (problems.hasErrors())
			return;

		final ISWFWriter writer = writerFactory.createSWFWriter(swf, compression, targetSettings.isDebugEnabled(),
				targetSettings.isTelemetryEnabled());

		try {
			writer.writeTo(outputFile);
		} catch (IOException e) {
			throw new RuntimeException("Failed creating the SWF file", e);
		}
	}

	@Override
	protected CopyAction createCopyAction() {
		return new CopyAction() {

			@Override
			public WorkResult execute(CopyActionProcessingStream copyAction) {
				getProject().getLogger().info("Build artifact for {}", getProject().getName());
				configure();
				addDependencies();
				problems.addAll(configurator.getConfigurationProblems());
				if (!problems.hasErrors()) {
					build();
				}
				if (problems.hasErrors()) {
					ProblemFormatter formatter = new WorkspaceProblemFormatter(
							(Workspace) project.getWorkspace(), new CompilerProblemCategorizer(configurator.getCompilerProblemSettings()));
					ProblemPrinter printer = new ProblemPrinter(formatter, System.err);
					printer.printProblems(problems.getProblems());
					throw new BuildException("Fatal build problems");
				}
				return new WorkResult(){
				
					@Override
					public boolean getDidWork() {
						return true;
					}
				};
			}
		};
	}
}
