package fr.lenra.gradle.task;

import java.io.IOException;

import org.gradle.api.Project;
import org.gradle.api.tasks.diagnostics.AbstractReportTask;
import org.gradle.api.tasks.diagnostics.internal.ReportRenderer;
import org.gradle.api.tasks.diagnostics.internal.TextReportRenderer;

import fr.lenra.gradle.actionscript.target.AbstractTargetCompiler;
import fr.lenra.gradle.plugin.ActionScriptExtension;

public class TargetsTask extends AbstractReportTask {
	private TargetReportRenderer renderer = new TargetReportRenderer();

	@Override
	protected void generate(Project arg0) throws IOException {
		Project project = this.getProject();

		ActionScriptExtension ext = (ActionScriptExtension) project.getExtensions().getByName("actionscript");
		ext.getTargets().all(t -> {
			renderer.addTarget(t);
		});
	}

	@Override
	protected ReportRenderer getRenderer() {
		return renderer;
	}

	private static class TargetReportRenderer extends TextReportRenderer {
		public void addTarget(AbstractTargetCompiler target) {
			this.getTextOutput().println(target.getName());
		}
	}
}