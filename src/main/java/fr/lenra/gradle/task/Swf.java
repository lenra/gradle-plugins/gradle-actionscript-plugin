package fr.lenra.gradle.task;

import java.io.File;
import java.util.Set;

import org.gradle.api.tasks.Input;

public class Swf extends AbstractActionScriptArchiveTask {
	public static final String DEFAULT_EXTENSION = "swf";

	@Input
	private String mainClass;

	public Swf() {
		this.getArchiveExtension().set(DEFAULT_EXTENSION);
	}

	/**
	 * @return the mainClass
	 */
	public String getMainClass() {
		return mainClass;
	}

	/**
	 * @param mainClass the mainClass to set
	 */
	public void setMainClass(String mainClass) {
		this.mainClass = mainClass;
	}

	@Override
	protected void configure() {
		if (mainClass == null)
			throw new RuntimeException("The main class is not defined");
		super.configure();
		this.getProject().getLogger().debug("Main class : {}", mainClass);
		Set<File> srcDirs = getSourceDirs().getSrcDirs();
		// configurator.setMainDefinition(mainClass);
		String mainFile = mainClass.replaceAll("[.]", "/") + ".as";
		String path = srcDirs.stream()
				.map(f -> new File(f, mainFile))
				.filter(f -> f.exists())
				.map(File::getAbsolutePath)
				.findFirst()
				.orElse(null);
		if (path == null)
			throw new RuntimeException("The main class not found");
		configurator.setConfiguration(new String[] { path }, "file-specs");
	}
}
