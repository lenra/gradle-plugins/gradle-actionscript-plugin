package fr.lenra.gradle.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.royale.compiler.Messages;
import org.apache.royale.compiler.clients.JSConfiguration;
import org.apache.royale.compiler.clients.problems.ProblemQuery;
import org.apache.royale.compiler.codegen.as.IASWriter;
import org.apache.royale.compiler.codegen.js.IJSWriter;
import org.apache.royale.compiler.config.Configuration;
import org.apache.royale.compiler.config.Configurator;
import org.apache.royale.compiler.driver.IBackend;
import org.apache.royale.compiler.internal.codegen.js.JSWriter;
import org.apache.royale.compiler.internal.driver.mxml.jsc.MXMLJSCJSSWCBackend;
import org.apache.royale.compiler.internal.projects.RoyaleJSProject;
import org.apache.royale.compiler.internal.projects.RoyaleProjectConfigurator;
import org.apache.royale.compiler.internal.workspaces.Workspace;
import org.apache.royale.compiler.problems.ICompilerProblem;
import org.apache.royale.compiler.targets.ISWCTarget;
import org.apache.royale.compiler.targets.ITarget.TargetType;
import org.apache.royale.compiler.targets.ITargetSettings;
import org.apache.royale.swc.ISWC;
import org.apache.royale.swc.io.ISWCWriter;
import org.apache.royale.swc.io.SWCDirectoryWriter;
import org.apache.royale.swf.io.SizeReportWritingSWFWriter;
import org.gradle.api.tasks.TaskExecutionException;

import fr.lenra.gradle.sourceset.LanguageSourceSet;
import fr.lenra.gradle.task.AbsctractLanguageCompileTask;

public class AsJs extends AbsctractLanguageCompileTask {

	@Override
	protected void compile() {
		if (getSource().getFiles().isEmpty())
			return;
		
		this.getProject().getLogger().info("Compile start : files={}, destinationDir={}", this.getSource().getAsPath(), this.getDestinationDir().toPath());

		LanguageSourceSet sourceSet = this.getLanguageSourceSet();

		try {
			IBackend backend = new MXMLJSCJSSWCBackend();
	
			Workspace workspace = new Workspace();
			
			RoyaleJSProject project = new RoyaleJSProject(workspace, backend);

			// TODO: Make a task to download the playerglobal with a given version :
			// https://fpdownload.macromedia.com/get/flashplayer/updaters/32/playerglobal32_0.swc

			project.setUseParallelCodeGeneration(true);

			Configurator projectConfigurator = new RoyaleProjectConfigurator(JSConfiguration.class);

			projectConfigurator.setConfiguration(new String[0], "file-specs");
			projectConfigurator.enableAccessibility(true);
			projectConfigurator.optimize(true);
			projectConfigurator.setExcludeNativeJSLibraries(true);
			projectConfigurator.allowSourcePathOverlap(false);
			projectConfigurator.setOutput(getDestinationDir());
			projectConfigurator.enableDebugging(true, null);
			// projectConfigurator.setTargetPlayer(25, 0, 0);
			projectConfigurator.setIncludeSources(getSource().getFiles());
			projectConfigurator.applyToProject(project);
			projectConfigurator.addExternalLibraryPath(sourceSet.getCompileClasspath().getFiles());
			projectConfigurator.applyToProject(project);
			projectConfigurator.useNetwork(true);
			
			this.getProject().getLogger().debug("Create target settings");
			ITargetSettings targetSettings = projectConfigurator.getTargetSettings(TargetType.SWC);

			ProblemQuery problems = new ProblemQuery(projectConfigurator.getCompilerProblemSettings());

			this.getProject().getLogger().debug("Get configuration");
			Configuration config = projectConfigurator.getConfiguration();
			Messages.setLocale(config.getToolsLocale());
			project.apiReportFile = config.getApiReport();
			problems.addAll(projectConfigurator.getConfigurationProblems());

			this.getProject().getLogger().debug("Create SWC target");
			ISWCTarget swcTarget = project.createSWCTarget(targetSettings, null);
			swcTarget.getLibrarySWFTarget();

			Collection<ICompilerProblem> swcProblems = new ArrayList<ICompilerProblem>();
			this.getProject().getLogger().debug("Build swc");
			final ISWC swc = swcTarget.build(swcProblems);

			problems.addAll(swcProblems);
			
			this.getProject().getLogger().debug("Generation problems : {}", problems);

			getDestinationDir().mkdirs();
			
			this.getProject().getLogger().debug("Write to destination directory");
			// final IASWriter writer = project.getBackend().createWriter(project, problems, compilationUnit, true);
			// writer.writeTo(swc);
		} catch (InterruptedException/*  | IOException */ e) {
			throw new TaskExecutionException(this, e);
		}
		this.getProject().getLogger().info("Compile end");
	}
}