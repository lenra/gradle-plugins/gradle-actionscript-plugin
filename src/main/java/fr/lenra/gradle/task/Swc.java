package fr.lenra.gradle.task;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.royale.compiler.Messages;
import org.apache.royale.compiler.config.Configuration;
import org.apache.royale.compiler.problems.ICompilerProblem;
import org.apache.royale.compiler.targets.ISWCTarget;
import org.apache.royale.compiler.targets.ITarget.TargetType;
import org.apache.royale.compiler.targets.ITargetSettings;
import org.apache.royale.swc.ISWC;
import org.apache.royale.swc.io.ISWCWriter;
import org.apache.royale.swc.io.SWCWriter;
import org.apache.royale.swf.io.ISWFWriterFactory;
import org.apache.royale.swf.io.SizeReportWritingSWFWriter;



public class Swc extends AbstractActionScriptArchiveTask {
	public static final String DEFAULT_EXTENSION = "swc";

	public Swc() {
		getArchiveExtension().set(DEFAULT_EXTENSION);
	}

	@Override
	protected void configure() {
		super.configure();
		configurator.setIncludeSources(getSourceDirs().getFiles());
	}

	@Override
	protected void build() {
		this.getProject().getLogger().debug("Create SWC settings");
		ITargetSettings targetSettings = configurator.getTargetSettings(TargetType.SWC);

		this.getProject().getLogger().debug("Get configuration");
		Configuration config = configurator.getConfiguration();
		Messages.setLocale(config.getToolsLocale());
		project.apiReportFile = config.getApiReport();
		problems.addAll(configurator.getConfigurationProblems());

		problems.addAll(project.getFatalProblems());

		this.getProject().getLogger().debug("Create SWC target");

		final ISWFWriterFactory writerFactory = SizeReportWritingSWFWriter
				.getSWFWriterFactory(targetSettings.getSizeReport());

		try {
			ISWCTarget swcTarget = project.createSWCTarget(targetSettings, null);

			swcTarget.getLibrarySWFTarget();

			Collection<ICompilerProblem> swcProblems = new ArrayList<ICompilerProblem>();
			final ISWC swc = swcTarget.build(swcProblems);
			problems.addAll(swcProblems);

			final ISWCWriter swcWriter = new SWCWriter(config.getOutput(), targetSettings.useCompression(),
					targetSettings.isDebugEnabled(), targetSettings.isTelemetryEnabled(),
					targetSettings.getSWFMetadataDate(), targetSettings.getSWFMetadataDateFormat(), writerFactory);

			this.getProject().getLogger().debug("attempting to write swc");
			swcWriter.write(swc);
		} catch (IOException | InterruptedException e) {
			throw new RuntimeException("Failed creating the SWF file", e);
		}
		this.getProject().getLogger().debug("returned from writing swc");
	}
}
