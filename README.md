# Gradle ActionScript Plugin

A Gradle plugin to manage ActionScript projects. Can build the project to SWF and AIR applications (Android, iOS, desktop...).